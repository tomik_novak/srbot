/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef	SRBOTHAL_H_
#define SRBOTHAL_H_


// Include header files

// This feature includes board.h, because it uses the system i/o
#include "board.h"
// This feature includes hal.h, because it uses its functions
#include "hal.h"

// Settings

// Declare functions
// Use medial capitals notation
// The module can call the declared functions from moduleMain
// You should use a common prefix for all functions and global variables.

//declarations of macro functions

#define greenLedOn() palSetPad(GPIOA, GPIOA_LED12)
#define greenLedOff() palClearPad(GPIOA, GPIOA_LED12)
#define greenLedToggle() palTogglePad(GPIOA, GPIOA_LED12)

#define blueLedOn() palSetPad(GPIOC, GPIOC_LED14)
#define blueLedOff() palClearPad(GPIOC, GPIOC_LED14)
#define blueLedToggle() palTogglePad(GPIOC, GPIOC_LED14)

#define orangeLedOn() palSetPad(GPIOC, GPIOC_LED13)
#define orangeLedOff() palClearPad(GPIOC, GPIOC_LED13)
#define orangeLedToggle() palTogglePad(GPIOC, GPIOC_LED13)

#define getLeftEnc() palReadPad((GPIO_TypeDef *)GPIOA_BASE,GPIOA_ENCA)
#define getRightEnc() palReadPad((GPIO_TypeDef *)GPIOA_BASE,GPIOA_ENCB)

#define getFrontIRR() (!palReadPad((GPIO_TypeDef *)GPIOC_BASE,GPIOC_PROX1))
#define getBackIRR() (!palReadPad((GPIO_TypeDef *)GPIOB_BASE,GPIOB_PROX3))
#define getLeftIRR() (!palReadPad((GPIO_TypeDef *)GPIOA_BASE,GPIOA_PROX2))
#define getRightIRR() (!palReadPad((GPIO_TypeDef *)GPIOB_BASE,GPIOB_PROX4))

#define setLD1() palSetPad(GPIOB, GPIOB_LD1)
#define clearLD1() palClearPad(GPIOB, GPIOB_LD1)
#define setLD2() palSetPad(GPIOB, GPIOB_LD2)
#define clearLD2() palClearPad(GPIOB, GPIOB_LD2)
#define setLD3() palSetPad(GPIOB, GPIOB_LD3)
#define clearLD3() palClearPad(GPIOB, GPIOB_LD3)
#define setLD4() palSetPad(GPIOC, GPIOC_LD4)
#define clearLD4() palClearPad(GPIOC, GPIOC_LD4)
#define setLD5() palSetPad(GPIOD, GPIOD_LD5)
#define clearLD5() palClearPad(GPIOD, GPIOD_LD5)




// Declare extern global variables
// Use underscore separated lowercase notation



#endif /* EXAMPLE_H_ */
