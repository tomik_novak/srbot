/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

    This file is part of Robot Firmware.

    Robot Firmware is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Robot Firmware is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MCU_H_
#define MCU_H_

static uint32_t const PERIPHERAL_MEMORY = 0x40000000;
static uint32_t const PERIPHERAL_BITBAND = 0x42000000;

static uint32_t const CORE_MEMORY = 0xE000E000;

static uint32_t const BKP = 0x0006C00;
static uint32_t const PWR = 0x0007000;
static uint32_t const RCC = 0x0021000;

static uint32_t const SCB = 0x0000D00;

static uint32_t const RCC_APB1ENR = 0x1C;
static uint32_t const RCC_CSR = 0x24;
static uint32_t const PWR_CR = 0x00;
static uint32_t const BKP_RESETREASON = 0xBC;
static uint32_t const BKP_LASTRESETREASON = 0xB8;

static uint32_t const SCB_AIRCR = 0x0C;
static uint32_t const SCB_VTOR = 0x08;

static uint8_t const RCC_APB1ENR_PWREN = 28;
static uint8_t const RCC_APB1ENR_BKPEN = 27;
static uint8_t const RCC_CSR_RMVF = 24;
static uint8_t const RCC_CSR_PORRSTF = 27;
static uint8_t const RCC_CSR_SFTRSTF = 28;
static uint8_t const PWR_CR_DBP = 8;

#endif
