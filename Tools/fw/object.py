#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import struct
from fw import config
from collections import namedtuple

class Module:
    def __init__(self, name, project, build, loader_id, module_id):
        self.name = name
        self.project = project
        self.build = build
        self.loader_id = loader_id
        self.module_id = module_id
        self.type = "MODULE"
    def __str__(self):
        return "MODULE {0}".format(self.name)
    def info(self):
        return "MODULE {0}\n project: {1}\n config: {2}\n module id: {3:#04x}\n loader id: {4:#04x}".format(self.name, self.project, self.build, self.module_id, self.loader_id)

class Robot:
    def __init__(self, name, module_list):
        self.name = name
        self.module_list = module_list
        self.type = "ROBOT"
    def __str__(self):
        return "ROBOT {0}".format(self.name)
    def info(self):
        return "ROBOT {0}\n {1}".format(self.name, "\n ".join(self.module_list))

class ConfigObject(object):
    def __init__(self, sdo_id, name, struct_format, members):
        self.sdo_id = sdo_id
        self.name = name
        self.tuple = namedtuple(name, members)
        self.struct_format = struct_format
        self.type = "SDO"
    def __str__(self):
        return "SDO {0}".format(self.name)
    def info(self):
        return "SDO {0}\n id: {3}\n format: {1}\n {2}".format(self.name, self.struct_format, "\n ".join(self.tuple._fields), self.sdo_id)
    def toBinary(self, *args):
        return struct.pack(self.struct_format, *args)
    def binaryToString(self, binary):
        return str(self.tuple(*struct.unpack(self.struct_format, binary)))

class ProcessObject(object):
    def __init__(self, name, struct_format, members):
        self.name = name
        self.tuple = namedtuple(name, members)
        self.struct_format = struct_format
        self.type = "PDO"
    def __str__(self):
        return "PDO {0}".format(self.name)
    def info(self):
        return "PDO {0}\n format: {1}\n {2}".format(self.name, self.struct_format, "\n ".join(self.tuple._fields))
    def toBinary(self, *args):
        return struct.pack(self.struct_format, *args)
    def binaryToString(self, binary):
        if binary is None:
            return "No value"
        else:
            return str(self.tuple(*struct.unpack(self.struct_format, binary)))

class ObjectId(object):
    def __init__(self, obj_name, instance_name, obj_id):
        self.name = obj_name + "." + instance_name
        self.obj_name = obj_name
        self.obj_id = obj_id
        self.type = "ID"

class ProcessObjectId(object):
    def __init__(self, pdo, obj_id):
        self.pdo_id = obj_id.obj_id
        self.pdo = pdo
    def toBinary(self, *args):
        return self.pdo.toBinary(*args)
    def binaryToString(self, binary):
        return self.pdo.binaryToString(binary)    
class Objects:
    def find(self, name):
        if name in self.module_map:
            return self.module_map[name]
        if name in self.sdo_map:
            return self.sdo_map[name]
        if name in self.pdo_map:
            return self.pdo_map[name]
    def findPdo(self, name):
        if '.' not in name:
            name+=".default"
        return self.pdo_id_map[name]
    def __init__(self, robot, modules, sdos, pdos, pdo_ids):
        self.current_robot = robot
        self.modules = modules
        self.sdos = sdos
        self.pdos = pdos
        self.pdo_ids = pdo_ids
        self.module_map = {module.name: module for module in modules}
        self.sdo_map = {sdo.name: sdo for sdo in sdos}
        self.pdo_map = {pdo.name: pdo for pdo in pdos}
        self.pdo_id_map = {pdo_id.name: ProcessObjectId(self.pdo_map[pdo_id.obj_name], pdo_id) for pdo_id in pdo_ids}

objects = None

def getObjects():
    global objects
    if objects is None:
        objects = Objects(config.getRobot(), config.getModules(), config.getSdos(), config.getPdos(), config.getPdoIds())
    return objects
    
