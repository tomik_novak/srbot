#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
from fw.commtest import CanTestCase 
import fw.config as config
from fw.protocol import sdoInt
import fw.protocol as protocol
import time

class CommTimeoutTest(CanTestCase):
    '''
    Test that the node uses timeout for SDO
    '''
    node_id = config.getNodeId()
    
    def setUp(self):
        CanTestCase.setUp(self)
        self.canopen = protocol.Canopen(self.can)
        self.sdo = protocol.CanopenSdo(self.canopen)
    
    def testSdoWriteTimeout(self):
        # Write
        self.canopen.writeSdoShort(self.node_id, 0x21, 0x0208, 0x00, sdoInt(8))
        # Expect confirm
        self.canopen.expectSdoShort(self, self.node_id, 0x60, 0x0208, 0x00, sdoInt(0))
        time.sleep(1)
        # Expect timeout
        self.canopen.expectSdoShort(self, self.node_id, 0x80, 0x0208, 0x00, sdoInt(0x05040000))
        self.canopen.expectNothing(self)
        pass
    
    def testSdoReadTimeout(self):
        # Write
        self.canopen.writeSdoShort(self.node_id, 0x40, 0x0108, 0x00, sdoInt(0))
        # Expect confirm
        self.canopen.expectSdoShort(self, self.node_id, 0x41, 0x0108, 0x00, sdoInt(8))
        time.sleep(1)
        # Expect timeout
        self.canopen.expectSdoShort(self, self.node_id, 0x80, 0x0108, 0x00, sdoInt(0x05040000))
        self.canopen.expectNothing(self)
        pass