#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import fw.config as config
import fw.object as obj
import time
import fw.protocol as protocol
from fw.commtest import CanTestCase

class EncoderTest(CanTestCase):
    node_id = config.getNodeId()
    
    def __init__(self, m):
        CanTestCase.__init__(self, m)
        objects = obj.getObjects()
        self.get_position = objects.findPdo("position")
        self.set_position = objects.findPdo("position.set")
        self.get_angle = objects.findPdo("angle")
        self.set_angle = objects.findPdo("angle.set")
        self.get_encoders = objects.findPdo("encoders")
        self.get_velocity = objects.findPdo("velocity")
        self.set_velocity = objects.findPdo("velocity.set")
        self.get_pwm = objects.findPdo("pwm")
        self.set_pwm = objects.findPdo("pwm.set")
        self.get_action_velocity = objects.findPdo("velocity.action")
        self.get_error = objects.findPdo("error")
        self.get_error_sum = objects.findPdo("error.sum")
    
    def setUp(self):
        CanTestCase.setUp(self)
        self.canopen = protocol.Canopen(self.can)
        self.sdo = protocol.CanopenSdo(self.canopen)
    
    
    def writeReadPose(self, x, y, angle):
        time.sleep(0.1) # TODO: sleep prevents triggering a bug
        self.canopen.writePdo(self.set_position.pdo_id, self.set_position.toBinary(x, y))
        time.sleep(0.1)
        self.canopen.writePdo(self.set_angle.pdo_id, self.set_angle.toBinary(angle))
        
        self.canopen.expectEventualPdo(self, self.get_position.pdo_id, self.get_position.toBinary(x, y))
        self.canopen.expectEventualPdo(self, self.get_angle.pdo_id, self.get_angle.toBinary(angle))

    
    
    def testPosePdo(self):
        nmt = protocol.CanopenNmtMaster(self.can)
        time.sleep(0.1)
        nmt.setOperational(self.node_id)
        
        self.writeReadPose(111, 222, 333)
        self.writeReadPose(0, 0, 0)
        time.sleep(0.1)
        nmt.setPreoperational(self.node_id)
    
    def testPdos(self):
        nmt = protocol.CanopenNmtMaster(self.can)
        nmt.setOperational(self.node_id)

        self.canopen.expectEventualPdo(self, self.get_position.pdo_id, self.get_position.toBinary(0, 0))
        self.canopen.expectEventualPdo(self, self.get_angle.pdo_id, self.get_angle.toBinary(0))
        self.canopen.expectEventualPdo(self, self.get_encoders.pdo_id, self.get_encoders.toBinary(0, 0))
        self.canopen.expectEventualPdo(self, self.get_velocity.pdo_id, self.get_velocity.toBinary(0, 0))
        self.canopen.expectEventualPdo(self, self.get_pwm.pdo_id, self.get_pwm.toBinary(0, 0))
        self.canopen.expectEventualPdo(self, self.get_action_velocity.pdo_id, self.get_action_velocity.toBinary(0, 0))
        self.canopen.expectEventualPdo(self, self.get_error.pdo_id, self.get_error.toBinary(0, 0))
        self.canopen.expectEventualPdo(self, self.get_error_sum.pdo_id, self.get_error_sum.toBinary(0, 0))
        
        time.sleep(0.1)
        nmt.setPreoperational(self.node_id)