#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import fw.config as config
import struct
import fw.protocol as protocol
from fw.commtest import CanTestCase

class IRTest(CanTestCase):
    node_id = config.getNodeId()
    
    def setUp(self):
        CanTestCase.setUp(self)
        self.canopen = protocol.Canopen(self.can)
        self.sdo = protocol.CanopenSdo(self.canopen)
    
    def testIRChangedPdo(self):
        nmt = protocol.CanopenNmtMaster(self.can)
        nmt.setOperational(7)
        self.sdo.writeObject(0x07, 0x0101, 1, struct.pack("<H",0x1))
        self.canopen.expectPdo(self, 0x01, struct.pack("<B",0))
        self.sdo.writeObject(0x07, 0x0101, 1, struct.pack("<H",0x3))
        self.canopen.expectPdo(self, 0x03, struct.pack("<B",0))

