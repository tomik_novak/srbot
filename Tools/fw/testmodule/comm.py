#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import fw.config as config
import struct
from fw.protocol import sdoInt
import fw.protocol as protocol
from fw.commtest import CanTestCase

class CommTest(CanTestCase):
    '''Test the SDO and PDO'''
    node_id = config.getNodeId()
    
    def setUp(self):
        CanTestCase.setUp(self)
        self.canopen = protocol.Canopen(self.can)
        self.sdo = protocol.CanopenSdo(self.canopen)
    
    def testPdo(self):
        self.canopen.writePdo(0xFF, struct.pack("<8B",1,2,3,4,5,6,7,8))
        self.canopen.expectPdo(self, self.node_id, struct.pack("<8B",1,0,0xFF,0x55,5,6,7,8))
        self.canopen.expectPdo(self, 0x200 + self.node_id, struct.pack("<8B",2,0,0xFF,0x55,5,6,7,8))
        self.canopen.expectPdo(self, 0x400 - self.node_id, struct.pack("<8B",3,0,0xFF,0x55,5,6,7,8))
    
    def testSdoReadSimple(self):
        self.canopen.writeSdoShort(self.node_id, 0x40, 0x0101, 0x00, sdoInt(0))
        self.canopen.expectSdoShort(self, self.node_id, 0x4F, 0x0101, 0x00, sdoTestData(1, 0x0101, 0x00))
        
        self.canopen.writeSdoShort(self.node_id, 0x40, 0x0102, 0x00, sdoInt(0))
        self.canopen.expectSdoShort(self, self.node_id, 0x4B, 0x0102, 0x00, sdoTestData(2, 0x0102, 0x00))
        
        self.canopen.writeSdoShort(self.node_id, 0x40, 0x0102, 0x5A, sdoInt(0))
        self.canopen.expectSdoShort(self, self.node_id, 0x4B, 0x0102, 0x5A, sdoTestData(2, 0x0102, 0x5A))
    
    def testSdoWriteRead(self):
        for length in range(1, 17):
            self.sdo.writeObject(self.node_id, 0x0300 + length, 0x00, sdoTestData(length, 0x0300 + length, 0x00))
        for length in range(1, 17):
            self.sdo.expectObject(self, self.node_id, 0x0300 + length, 0x00, sdoTestData(length, 0x0300 + length, 0x00))
    
    def testSdoReadReadonly(self):
        for length in range(1, 17):
            self.sdo.expectObject(self, self.node_id, 0x0100 + length, 0x00, sdoTestData(length, 0x0100 + length, 0x00))
    
    def testSdoWriteWriteonly(self):
        for length in range(1, 17):
            self.sdo.writeObject(self.node_id, 0x0200 + length, 0x00, sdoTestData(length, 0x0200 + length, 0x00))

    def testSdoReadWriteonly(self):
        self.canopen.writeSdoShort(self.node_id, 0x40, 0x0201, 0x00, sdoInt(0))
        self.canopen.expectSdoShort(self, self.node_id, 0x80, 0x0201, 0x00, sdoInt(0x06010001))
        
    def testSdoWriteReadonly(self):
        self.canopen.writeSdoShort(self.node_id, 0x23, 0x0101, 0x00, sdoInt(0))
        self.canopen.expectSdoShort(self, self.node_id, 0x80, 0x0101, 0x00, sdoInt(0x06010002))
        
    def testSdoReadNotExist(self):
        '''
        Test that the module returns correct error code for trying to read
        an object with no handler.
        '''
        self.canopen.writeSdoShort(self.node_id, 0x40, 0x0601, 0x00, sdoInt(0))
        self.canopen.expectSdoShort(self, self.node_id, 0x80, 0x0601, 0x00, sdoInt(0x06020000))
        
    def testSdoWriteNotExist(self):
        '''
        Test that the module returns correct error code for trying to write
        an object with no handler.
        '''
        self.canopen.writeSdoShort(self.node_id, 0x23, 0x0601, 0x00, sdoInt(0))
        self.canopen.expectSdoShort(self, self.node_id, 0x80, 0x0601, 0x00, sdoInt(0x06020000))
        
    def testSdoWriteBadLength(self):
        self.canopen.writeSdoShort(self.node_id, 0x23, 0x0205, 0x00, sdoInt(0))
        self.canopen.expectSdoShort(self, self.node_id, 0x80, 0x0205, 0x00, sdoInt(0x06070013))
        self.canopen.writeSdoShort(self.node_id, 0x21, 0x0205, 0x00, sdoInt(10))
        self.canopen.expectSdoShort(self, self.node_id, 0x80, 0x0205, 0x00, sdoInt(0x06070012))
        
    def testSdoWriteBadData(self):
        sdodata = struct.pack("<BBBB", sdoTestByte(0, 0x0204, 0x00), 0xFF, 0xFF, 0xFF)
        self.canopen.writeSdoShort(self.node_id, 0x23, 0x0204, 0x00, sdodata)
        self.canopen.expectSdoShort(self, self.node_id, 0x80, 0x0204, 0x00, sdoInt(0x06090030))

def sdoTestByte(pos, index, subindex):
    return (subindex + (index + pos * 33) * 33) & 0xFF;

def sdoTestData(length, index, subindex):
    return ''.join([chr(sdoTestByte(i, index, subindex)) for i in range(length)])
