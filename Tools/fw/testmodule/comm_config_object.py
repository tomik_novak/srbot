#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

from fw.commtest import CanTestCase
import fw.config as config
import fw.protocol as protocol
from comm import sdoTestData
from fw.protocol import sdoInt

class CommConfigObjectTest(CanTestCase):
    '''Test SDO config objects'''
    node_id = config.getNodeId()
    
    lengths = [12, 10, 21, 1]
    parts = [[4,8,12],[10],[1,3,6,10,15,21],[1]]
    
    def setUp(self):
        CanTestCase.setUp(self)
        self.canopen = protocol.Canopen(self.can)
        self.sdo = protocol.CanopenSdo(self.canopen)
    
    def testBadLength(self):
        self.canopen.writeSdoShort(self.node_id, 0x21, 0x0401, 0x00, sdoInt(11))
        self.canopen.expectSdoShort(self, self.node_id, 0x80, 0x0401, 0x00, sdoInt(0x06070013))
        self.canopen.writeSdoShort(self.node_id, 0x21, 0x0401, 0x00, sdoInt(13))
        self.canopen.expectSdoShort(self, self.node_id, 0x80, 0x0401, 0x00, sdoInt(0x06070012))
 
    def testSimpleObject(self):
        for i in range(0,4):
            self.sdo.writeObject(self.node_id, 0x0401 + i, 0x00, sdoTestData(self.lengths[i],0x0401 + i, 0x00))
            self.sdo.expectObject(self, self.node_id, 0x0401 + i, 0x00, sdoTestData(self.lengths[i], 0x0401 + i, 0x00))
    
    def readObjectParts(self, object_id, parts, data):
        self.sdo.expectObject(self, self.node_id, object_id, 0x00, data)
        last = 0
        for i in range(0, len(parts)):
            offset = parts[i]
            print offset, last
            self.sdo.expectObject(self, self.node_id, object_id, i + 1, data[last:offset])
            last = offset
    
    def writeReadObjectParts(self, object_id, length, parts):
        data = bytearray(sdoTestData(length, object_id, 0x00))
        # Write whole object
        self.sdo.writeObject(self.node_id, object_id, 0, bytes(data))
        self.readObjectParts(object_id, parts, bytes(data))
        # Write parts
        last = 0
        for i in range(0, len(parts)):
            offset = parts[i]
            part = sdoTestData(offset-last, object_id, i + 1)
            self.sdo.writeObject(self.node_id, object_id, i + 1, part)
            data[last:offset] = part
            self.readObjectParts(object_id, parts, bytes(data))
            last = offset
    
    def testObjectParts(self):
        for i in range(0,4):
            self.writeReadObjectParts(0x0401+i, self.lengths[i], self.parts[i])
            
            