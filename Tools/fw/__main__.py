#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

'''
Firmware testing and manipulation
    build_project MODULE_PROJECT [CONF ID]
    upload_project MODULE_PROJECT [MODE]
    build MODULE_NAME
    upload MODULE_NAME
    reset MODULE_NAME [reason]
    enable MODULE_NAME
    disable MODULE_NAME
    info      - Show configuration
    serial_info [all] [diag]- Show information about serial
    can_info [reason] [ID...] - Show information about can
    help
'''

import sys
import struct
import fw.config as config
import fw.upload as upload
import fw.protocol as protocol
import fw.comm as comm
import fw.diagnostics as diagnostics
import fw.object as obj

def compileModule(mod, conf = None, module_id = None):
    compiler = upload.createFirmwareCompiler()
    module = obj.getObjects().find(mod)
    try:
        if conf is None:
            conf = module.build
        if module_id is None:
            module_id = str(module.module_id)
        confvars = {}
        confvars['CONF_BUILD'] = conf
        confvars['CONF_MODULEID'] = module_id
        compiler.compile(module.project, confvars)
    finally:
        compiler.close() 

def compileFirmware(mod, conf = None, module_id = None):
    compiler = upload.createFirmwareCompiler()
    try:
        confvars = {}
        if conf is not None:
            confvars['CONF_BUILD'] = conf
        if module_id is not None:
            confvars['CONF_MODULEID'] = module_id
        compiler.compile(mod, confvars)
    finally:
        compiler.close() 

def uploadFirmware(mod, mode = "app"):
    uploader = upload.createFirmwareUploader()
    try:
        uploader.setMode(mode)
        uploader.upload(mod) 
    finally:
        uploader.close()

def uploadModule(mod):
    uploader = upload.createFirmwareUploader()
    module = obj.getObjects().find(mod)
    try:
        uploader.setMode("app")
        uploader.upload(module.project, module.module_id, module.loader_id) 
    finally:
        uploader.close()

def buildUploadModule(mod):
    compileModule(mod)
    uploadModule(mod)

def reset(mod, reason = "0x101"):
    timeout = comm.Timeout(1)
    module = obj.getObjects().find(mod)
    try:
        with comm.createCan(timeout) as can:
            nmt = protocol.CanopenNmtMaster(can)
            nmt.reset(module.module_id, eval(reason))
            timeout.time = 3
            if not can.readDisconnect():
                print 'Device did not reset'
            else:
                print 'Device reset'
    finally:
        timeout.close()

def enable(mod):
    timeout = comm.Timeout(1)
    module = obj.getObjects().find(mod)
    try:
        with comm.createCan(timeout) as can:
            nmt = protocol.CanopenNmtMaster(can)
            nmt.setOperational(module.module_id)
    finally:
        timeout.close()

def disable(mod):
    timeout = comm.Timeout(1)
    module = obj.getObjects().find(mod)
    try:
        with comm.createCan(timeout) as can:
            nmt = protocol.CanopenNmtMaster(can)
            nmt.setPreoperational(module.module_id)
    finally:
        timeout.close()

def info():
    print("Workspace: {0}".format(config.getWorkspace()))
    print("Compiler: {0}".format(config.getCompiler()))
    print("Uploader: {0}".format(config.getUploader()))

def serialInfo(*args):
    print("Primary serial {0}".format(config.getPrimarySerial()))
    print("Secondary serial {0}".format(config.getSecondarySerial()))
    print("Loader serial {0}".format(config.getLoaderSerial()))
    
    timeout = comm.Timeout(1)
    try:
        
        serials = [("primary", comm.createPrimarySerial)]
        if 'all' in args:
            serials += [("secondary", comm.createSecondarySerial)]
        for (serial_name, serial_func) in serials:
            with serial_func(timeout) as serial:
                serial.writeCommand('l', 't')
                response = serial.readCommand('a')
                if response is not None and response[0] == 'a':
                    print("On {1} serial {0}".format(response[1], serial_name))
                if 'diag' in args:
                    diag = diagnostics.SerialDiagnostics(serial)
                    diag.getDiagnostics()
                    diag.output()
    finally:
        timeout.close()

def canInfo(*args):
    print("CAN {0}".format(config.getCan()))
    timeout = comm.Timeout(1)
    try:
        with comm.createCan(timeout) as can:
            nmt = protocol.CanopenNmtMaster(can)
            ids = []
            for arg in args:
                if arg.startswith('0x'):
                    ids += [int(arg, 16)]
            if 'reason' in args:
                for node_id in ids:
                    try:
                        reason = hex(nmt.getResetReason(node_id))
                    except protocol.CanopenException:
                        reason = "unknown"
                    print "{0:x} reason {1}".format(node_id, reason)
    finally:
        timeout.close()

def readLoop(module_name, pdo_name):
    objects = obj.getObjects()
    module = objects.find(module_name)
    pdo = objects.findPdo(pdo_name)
    timeout = comm.Timeout(1)
    try:
        with comm.createCan(timeout) as can:
            nmt = protocol.CanopenNmtMaster(can)
            nmt.setOperational(module.module_id)
            try:
                while True:
                    value = nmt.canopen.readPdo(pdo.pdo_id)
                    if value is None:
                        print "No reply"
                    else:
                        print pdo.binaryToString(value)
            except KeyboardInterrupt:
                print "Interrupted by user"
                nmt.setPreoperational(module.module_id)
    finally:
        timeout.close()

def objectInfo(name = None):
    objects = obj.getObjects()
    robot = config.getRobot()
    if name is None:
        for mod in objects.modules:
            flags = []
            if mod.name == robot:
                flags += ["DEFAULT"]
            print str(mod), " ".join(flags)
        for o in objects.pdos:
            print str(o)
        for o in objects.sdos:
            print str(o)    
    else:
        o = objects.find(name)
        print o.info()

def read(name, *args):
    objects = obj.getObjects()
    path = name.split(".")
    o = objects.find(path[0])
    if o.type == "SDO":
        print "Specify the module"
    elif o.type == "MODULE":
        sdo_o = objects.find(path[1])
        timeout = comm.Timeout(1)
        try:
            with comm.createCan(timeout) as can:
                sdo = protocol.CanopenSdo(protocol.Canopen(can))
                print sdo_o.binaryToString(sdo.readObject(o.module_id, sdo_o.sdo_id, 0))
        finally:
            timeout.close()
    elif o.type == "PDO":
        pdo_o = objects.findPdo(name)
        timeout = comm.Timeout(1)
        try:
            with comm.createCan(timeout) as can:
                canopen = protocol.Canopen(can)
                print pdo_o.binaryToString(canopen.readPdo(pdo_o.pdo_id))
        finally:
            timeout.close()
    else:
        print "Unknown object type"

def write(name, *args):
    objects = obj.getObjects()
    path = name.split(".")
    o = objects.find(path[0])
    if o.type == "SDO":
        print "Specify the module"
    elif o.type == "MODULE":
        sdo_o = objects.find(path[1])
        timeout = comm.Timeout(1)
        try:
            with comm.createCan(timeout) as can:
                sdo = protocol.CanopenSdo(protocol.Canopen(can))
                sdo.writeObject(o.module_id, sdo_o.sdo_id, 0, sdo_o.toBinary(*(eval(a) for a in args)))
        finally:
            timeout.close()
    elif o.type == "PDO":
        pdo_o = objects.findPdo(name)
        timeout = comm.Timeout(1)
        try:
            with comm.createCan(timeout) as can:
                canopen = protocol.Canopen(can)
                canopen.writePdo(pdo_o.pdo_id, pdo_o.toBinary(*(eval(a) for a in args)))
        finally:
            timeout.close()
    else:
        print "Unknown object type"

def encoderInfo(module_name = "encoder_dev"):
    objects = obj.getObjects()
    module = objects.find(module_name)
    timeout = comm.Timeout(1)
    encoder_pdo = objects.findPdo("encoders")
    pos_pdo = objects.findPdo("position")
    ang_pdo = objects.findPdo("angle")
    vel_pdo = objects.findPdo("velocity")
    try:
        with comm.createCan(timeout) as can:
            nmt = protocol.CanopenNmtMaster(can)
            nmt.setOperational(module.module_id)
            try:
                while True:
                    print encoder_pdo.binaryToString(nmt.canopen.readPdo(encoder_pdo.pdo_id))
                    print pos_pdo.binaryToString(nmt.canopen.readPdo(pos_pdo.pdo_id))
                    print ang_pdo.binaryToString(nmt.canopen.readPdo(ang_pdo.pdo_id))
                    print vel_pdo.binaryToString(nmt.canopen.readPdo(vel_pdo.pdo_id))
            except KeyboardInterrupt:
                print "Interrupted by user"
                nmt.setPreoperational(module.module_id)
    finally:
        timeout.close()
    
    
def commandHelp():
    print(__doc__)


commands = {
    'build_project':compileFirmware,
    'upload_project':uploadFirmware,
    'build':compileModule,
    'upload':uploadModule,
    'build_upload':buildUploadModule,
    'reset':reset,
    'enable':enable,
    'disable':disable,
    'info':info,
    'serial_info':serialInfo,
    'can_info':canInfo,
    'object_info':objectInfo,
    'encoder_info':encoderInfo,
    'read':read,
    'read_loop':readLoop,
    'write':write,
    'help':commandHelp,
}

if sys.argv[1] in commands:
    commands[sys.argv[1]](*sys.argv[2:])
else:
    commandHelp()
