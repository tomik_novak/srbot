#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import struct

# Helper functions
def sdoInt(integer):
    return struct.pack("<I", integer)
# Exceptions
class CanopenException(Exception):
    pass
# Classes
class Canopen(object):
    '''Use and test CANOpen communication. This class doesn't claim compliance with the CANOpen specification'''
    def checkNodeId(self, node_id):
        if not 0 <= node_id < 0x80: 
            raise CanopenException("Invalid node id {0:x} specified".format(node_id))
    def checkPdoId(self, pdo_id):
        if not 0 <= pdo_id < 0x400: 
            raise CanopenException("Invalid PDO id {0:x} specified".format(pdo_id))
    
    def checkSid(self, frame_sid, expected_sid):
        if frame_sid != expected_sid:
            raise CanopenException("Unexpected SID {0:x}".format(frame_sid))

    def checkCommand(self, command):
        if not 0 <= command < 0x100:
            raise CanopenException("Invalid command {0:x} specified".format(command))
 
    def expectNothing(self, test):
        '''Assert there is no input'''
        self.can.expectNothing(test)
    
    def writeNmt(self, node_id, command):
        self.checkNodeId(node_id)
        self.can.writeFrame(0x000, struct.pack("<BB", command, node_id))
    
    def readNmt(self, node_id):
        self.checkNodeId(node_id)
        frame = self.can.readFrame([0x700 + node_id])
        if frame is None:
            return None
        frame_sid, frame_data = frame
        self.checkSid(frame_sid, 0x700 + node_id)
        return struct.unpack("<B", frame_data)[0]
        
    def writePdo(self, pdo_id, data):
        '''Write a PDO frame to the CAN'''
        self.checkPdoId(pdo_id)
        self.can.writeFrame(pdo_id + 0x180, data)
        
    def readPdo(self, pdo_id):
        '''Read a CAN frame and interpret it as a PDO frame'''
        self.checkPdoId(pdo_id)
        frame = self.can.readFrame([0x180 + pdo_id])
        if frame is None:
            return None
        frame_sid, frame_data = frame
        self.checkSid(frame_sid, 0x180 + pdo_id)
        return frame_data
    
    def expectEventualPdo(self, test, pdo_id, data):
        self.checkPdoId(pdo_id)
        with self.can.timeout:
            while not self.can.timeout.elapsed():
                frame = self.can.readFrame([pdo_id + 0x180])
                if frame is not None:
                    frame_sid, frame_data = frame
                    if frame_sid == pdo_id+0x180 and frame_data==data:
                        return
        test.fail('PDO with the expected value did not arrive')
        
    def expectPdo(self, test, pdo_id, data):
        '''Read a PDO frame and assert it to be equal to the specified PDO frame'''
        self.checkPdoId(pdo_id)
        self.can.expectFrame(test, pdo_id + 0x180, data)
        
    def writeSdoShort(self, node_id, command, index, subindex, data):
        self.checkNodeId(node_id)
        self.checkCommand(command)
        if not 0 <= index < 0x10000:
            raise CanopenException("Invalid object index")
        if not 0 <= subindex < 0x100:
            raise CanopenException("Invalid sub-index")
        frame_data = struct.pack("<BHB4s", command, index, subindex, data)
        self.can.writeFrame(node_id + 0x600, frame_data)
    
    def writeSdoLong(self, node_id, command, data):
        self.checkNodeId(node_id)
        self.checkCommand(command)
        frame_data = struct.pack("<B7s", command, data)
        self.can.writeFrame(node_id + 0x600, frame_data)
    
    def readSdoShort(self, node_id):
        '''Read a CAN frame and interpret it as a short SDO frame'''
        self.checkNodeId(node_id)
        frame = self.can.readFrame([0x580 + node_id])
        if frame is None:
            return None
        frame_sid, frame_data = frame
        if len(frame_data) != 8:
            raise CanopenException("Invalid SDO length")
        self.checkSid(frame_sid, 0x580 + node_id) 
        command, index, subindex, data = struct.unpack('<BHB4s', frame_data)
        return command, index, subindex, data
    
    def readSdoLong(self, node_id):
        '''Read a CAN frame and interpret it as a long SDO frame'''
        frame = self.can.readFrame([0x580 + node_id])
        if frame is None:
            return None
        frame_sid, frame_data = frame
        if len(frame_data) != 8:
            raise CanopenException("Invalid SDO length")
        command, data = struct.unpack('<B7s', frame_data)
        self.checkSid(frame_sid, 0x580 + node_id)
        return command, data
    
    def expectSdoShort(self, test, node_id, command, index, subindex, data):
        test.assertEqual(self.readSdoShort(node_id), (command, index, subindex, struct.pack("<4s",data)))
    
    def expectSdoLong(self, test, node_id, command, data):
        test.assertEqual(self.readSdoShort(node_id), (command, data))
    

    def __init__(self, can):
        self.can = can

class CanopenSdo(object):
    def checkReplyShort(self, index, subindex, rindex, rsubindex):
        if (rindex, rsubindex) != (index, subindex):
            raise CanopenException("Reply from wrong object")
    
    def checkReplyCommand(self, cmd, reply_cmd, message):
        if reply_cmd != cmd:
            raise CanopenException("Wrong command reply {0:x} to {1}".format(reply_cmd, message))
    
    def segmentRange(self, datalen):
        return range(0,(datalen+6)/7)
    
    def writeObject(self, node_id, index, subindex, data):
        datalen = len(data)
        if datalen == 0:
            raise CanopenException("Zero-length object")
        elif datalen <= 4:
            sdocmd = 0x23 | ((4-datalen)<<2)
            self.canopen.writeSdoShort(node_id, sdocmd, index, subindex, data)
            response = self.canopen.readSdoShort(node_id)
            if response is None:
                raise CanopenException("No reply")
            reply_cmd, rindex, rsubindex, reply_data = response
            self.checkReplyShort(index, subindex, rindex, rsubindex)
            self.checkReplyCommand(0x60, reply_cmd,'expedited write')
            if reply_data != sdoInt(0):
                raise CanopenException("Data in reply")
        else:
            self.canopen.writeSdoShort(node_id, 0x21, index, subindex, sdoInt(datalen))
            reply = self.canopen.readSdoShort(node_id)
            if reply is None:
                raise CanopenException("No reply")
            (reply_cmd, rindex, rsubindex, reply_data) = reply 
            self.checkReplyShort(index, subindex, rindex, rsubindex)
            if reply_cmd != 0x60:
                raise CanopenException("Wrong command reply {0:x} to segmented write".format(reply_cmd))
            if reply_data != sdoInt(0):
                raise CanopenException("Data in reply")
            
            for cycle in self.segmentRange(datalen):
                sdocmd = 0x00 if (cycle%2 == 0) else 0x10
                notData = (cycle+1)*7-datalen
                segmentlen = 7
                if notData >= 0:
                    sdocmd |= 1 | (notData<<1)
                    segmentlen -= notData
                self.canopen.writeSdoLong(node_id, sdocmd, data[cycle*7:cycle*7 + segmentlen])
                
                sdocmd = 0x20 if (cycle%2 == 0) else 0x30
                reply = self.canopen.readSdoLong(node_id)
                if reply is None:
                    raise CanopenException("No reply")
                reply_cmd, reply_data = reply
                if reply_cmd != sdocmd:
                    raise CanopenException("Wrong reply during segmented write".format(reply_cmd))
                
    
    def expectObject(self, test, node, index, subindex, data):
        actual_data = self.readObject(node, index, subindex)
        test.assertEqual(actual_data, data)
    
    def readObject(self, node_id, index, subindex):
        self.canopen.writeSdoShort(node_id, 0x40, index, subindex, sdoInt(0))
        response = self.canopen.readSdoShort(node_id)
        if response is None:
            raise CanopenException("No reply from node")
        cmd, rindex, rsubindex, data = response
        self.checkReplyShort(index, subindex, rindex, rsubindex)
        
        if (cmd & 0xF3) == 0x43:
            datalen = 4 - ((cmd >> 2) & 0x3) 
            return data[0:datalen]
        elif cmd == 0x41:
            datalen = struct.unpack("<I", data)[0]
            data = ''
            if datalen == 0:
                raise CanopenException("Zero-length object returned")
            for cycle in self.segmentRange(datalen):
                sdocmd = 0x60 if (cycle%2 == 0) else 0x70
                self.canopen.writeSdoLong(node_id, sdocmd, '')
                sdocmd = 0x00 if (cycle%2 == 0) else 0x10
                notData = (cycle+1)*7-datalen
                segmentlen = 7
                if notData >= 0:
                    sdocmd |= 1 | (notData<<1)
                    segmentlen -= notData
                reply = self.canopen.readSdoLong(node_id)
                if reply is None:
                    raise CanopenException("No reply from node")
                reply_cmd, rdata = reply
                if reply_cmd != sdocmd:
                    raise CanopenException("Wrong reply during segmented read".format(reply_cmd))
                data += rdata[0:segmentlen]
            return data
        else:
            raise CanopenException("Wrong command reply to read")
        
    def __init__(self, canopen):
        self.canopen = canopen


class CanopenNmtMaster(object):
    reset_object = 0x3E01
    def resetToLoader(self, address):
        self.reset(address, 0x0102)
    def resetToApplication(self, address):
        self.reset(address, 0x0101)
    def reset(self, address, reason):
        self.sdo.writeObject(address, self.reset_object, 0, struct.pack("<H",reason))
        self.setState(address, 0x81)
    def getResetReason(self, address):
        return struct.unpack("<H", self.sdo.readObject(address, self.reset_object, 0))[0]
    def setPreoperational(self, address):
        self.setState(address, 0x80)
    def setOperational(self, address):
        self.setState(address, 0x01)
    def setState(self, address, mode):
        self.canopen.writeNmt(address, mode)
    #TODO: def getState(self, address):
    #    pass
    def stateToString(self, state):
        if state == 0x05:
            return "operational"
        elif state == 0x7F:
            return "pre-operational"
        elif state == 0x00:
            return "boot"
        else:
            return "unknown {0:#x}".format(state)
    def __init__(self, can):
        self.canopen = Canopen(can)
        self.sdo = CanopenSdo(self.canopen)