#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import fw.protocol as protocol
import fw.config as config
import fw.upload as upload
from fw.commtest import CanTestCase

class NmtHeartbeatTest(CanTestCase):
    '''
    Test the heartbeat feature of a module.
    Related configuration:
     nodeId : id of the module which is tested
     can : can bus used for communication
    Setting:
     take a module compiled with COMM_HEARTBEAT set to non-zero
     connect the module via can bus or compatible
    '''
    node_id = config.getNodeId()
    
    def setUp(self):
        CanTestCase.setUp(self)
        self.canopen = protocol.Canopen(self.can)
    
    def testHeartbeat(self):
        '''
        Test that the module sends heartbeat frames.
        Waits for 10 heartbeat frames from the module.
        Checks that the state of the module is one of known values.
        '''
        count = 0
        bad = 0
        while count < 10 and bad < 20:
            state = self.canopen.readNmt(self.node_id)
            if state is not None:
                self.assertIn(state, [0x00, 0x05, 0x7F])
                count += 1
            else:
                bad += 1
        self.assertEqual(count, 10)
    
    def waitTransition(self, old, new):
        bad = 0
        while bad < 20:
            state = self.canopen.readNmt(self.node_id)
            if state is not None:
                self.assertIn(state, [old, new])
                if state == new:
                    return
                else:
                    bad += 1
            else:
                bad += 1
        self.fail("Waiting too long for state transition")
               
    def testHeartbeatState(self):
        '''
        Test that the module supports state transitions.
        Waits until the device enters pre-operational state.
         (boot message is accepted but not required)
        Sends a NMT command to the module to enter operational state.
        Waits until the device enters operational state,
        Sends a NMT command to the module to enter pre-operational state.
        Waits until the device enters pre-operational state.
        '''
        nmt = upload.CanopenNmtMaster(self.can)
        self.waitTransition(0x00, 0x7F)
        nmt.setOperational(self.node_id)
        self.waitTransition(0x7F, 0x05)
        nmt.setPreoperational(self.node_id)
        self.waitTransition(0x05, 0x7F)
        