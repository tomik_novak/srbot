#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import fw.comm as comm
from fw.commtest import TimedTestCase

class SerialResetTest(TimedTestCase):
    '''
    Test that both the application and the loader support reset commands.
    Take a module with enabled serial (or USB) and connect it via serial (or USB).
    This test can be run in loader or application mode.
    '''
    
    def checkAndReset(self, test_pattern, reset_command, print_message):
        with comm.createPrimarySerial(self.timeout) as serial:
            if test_pattern is not None:
                serial.writeCommand('l','t')
                serial.expectCommandPattern(self, 'a', test_pattern)
            if print_message is not None:
                print(print_message)
            if reset_command is not None:
                serial.writeCommand('l', reset_command)
                serial.expectDisconnect(self)

    def testReset(self):
        '''Test reset from both loader and app to both loader and app'''
        self.checkAndReset(None, 'l', 'Initial reset into loader')
        self.checkAndReset('LOA[a-f0-9]{2}', 'l', 'Reset from loader into loader')
        self.checkAndReset('LOA[a-f0-9]{2}', 'a', 'Reset from loader into app')
        self.checkAndReset(None, 'a', 'Reset from app into app')
        self.checkAndReset(None, 'l', 'Reset from app into loader')
        self.checkAndReset('LOA[a-f0-9]{2}', None, None)
