#     Copyright (C) 2014  Vlastimil Dort
#
#     This file is part of Robot Firmware.
#
#     Robot Firmware is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     Robot Firmware is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
# 
#     You should have received a copy of the GNU General Public License
#     along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.

import unittest

import fw.comm as comm
import fw.protocol as protocol
import fw.upload as upload
import fw.config as config

class SdoLoaderLoadTest(unittest.TestCase):
    '''
    Test loader SDO commands.
    This test should pass only in application mode.
    '''
    
    def setUp(self):
        self.uploader = upload.CanopenFirmwareUploader()
        self.compiler = upload.createFirmwareCompiler()

    def tearDown(self):
        self.uploader.close()
        self.compiler.close()

    def checkTestToken(self, node_id, test_token):
        '''Connect to the module and verify the test token'''
        self.uploader.timeout.time = 1
        with comm.createCan(self.uploader.timeout) as can:
            sdo = protocol.CanopenSdo(protocol.Canopen(can))
            sdo.expectObject(self, node_id, 0x3E05, 0x00, protocol.sdoInt(test_token))
       
    def _loadAndCheck(self, firmware, node_id, test_token, build_conf):
        print('{0} ({1:02x}) with {2:08x}'.format(firmware, node_id, test_token))
        self.compiler.compile(firmware, {'CONF_MODULEID':'{0:#x}'.format(node_id), 'CONF_BUILD' : build_conf, 'CONF_TESTTOKEN' : '{0:#x}'.format(test_token)})
        self.uploader.upload(firmware)
        self.checkTestToken(node_id, test_token)
    
    def testApplicationLoad(self):
        '''Upload two different applications and check their responses'''
        self.uploader.setMode('app')
        node_id = config.getNodeId()
        bconf = config.getBuildConfiguration()
        
        self._loadAndCheck('TestModule', node_id, 0xF0F0F0F0, bconf)
        self._loadAndCheck('TestModule', node_id, 0x5A5A5A5A, bconf)
       
        self.compiler.clean('TestModule')
        
    def testLoaderLoad(self):
        '''Upload two different loaders and check their responses'''
        self.uploader.setMode('loader')
        node_id = config.getLoaderNodeId()
        bconf = config.getBuildConfiguration()
        
        self._loadAndCheck('Loader', node_id, 0x3C3C3C3C, bconf)
        self._loadAndCheck('Loader', node_id, 0x69696969, bconf)
        
        self.compiler.clean('Loader')