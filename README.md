# Robot Firmware

Firmware for mobile robots

## Installation

### Required software

* GCC compiler for ARM ([download binaries](https://launchpad.net/gcc-arm-embedded))
* OpenOCD 0.7 (for example distribution package in [Jessie](https://packages.debian.org/jessie/openocd))
* Python 2.7 (for example distribution package in [Jessie](https://packages.debian.org/jessie/python2.7))
* Pyserial (for example distribution package in [Jessie](https://packages.debian.org/jessie/python-serial))
* ChibiOS/RT 2.6 ([download](http://chibios.org/))

### Environment setup

Before running `make`, `python` or `eclipse`, set these environment variables:

* `ARMGCC` to the root directory of GCC for ARM
* `OPENOCD` to the directory where the `openocd` binary is located
* `CHIBIOS` to the root directory of ChibiOS/RT

You can also add the path to `Tools` to the `PYTHONPATH` variable, to allow running the Python scripts from any directory.

The Python scripts use local configuration from `Tools/fw/local.cfg`. 
Running `python -mfw info` will create this file with default values.

### Eclipse

Eclipse is not required to use Robot Firmware.

To use the Eclipse projects, install:

* Eclipse 4.3 ([download](http://www.eclipse.org/downloads))
* CDT  (part of Eclipse IDE for C/C++ Developers)
* PyDev (http://pydev.org, update site http://pydev.org/updates/)
* GNU ARM Eclipse plugin (http://gnuarmeclipse.sourceforge.net, update site http://gnuarmeclipse.sourceforge.net/updates)
* EGit (Eclipse Git Team Provider)

Make sure to set the environment variables before starting Eclipse. Additionally, you must set the variable CHIBIOS 
again in *Window* > *Preferences* > *General* > *Workspace* > *Linked resources*.

#### Downloading with EGit

In *Git Repositories* view, click *Clone a Git Repository...*. Paste `https://bitbucket.org/panacekcz/firmware.git` to the Host field.

The projects in the repository do not contain the C/C++ settings, because the setting files are full of trash. However, there is a branch
`cdt_project`, which contains the preferred settings. To use it, open a terminal in the root of your cloned repository and run 
`git archive origin/cdt_project | tar -kxf -`. After that, right-click on the repository in *Git Repositories*, and select *Import Projects...*,
then select *Import existing projects*.

If you want to set-up the C/C++ settings on your own (not recommended), import the projects first, and then use
*File* > *New* > *Convert to a C/C++ Project*.

### Building

#### Switch
First, you should build *Switch*. You have two options:

* run `make all` in `Switch/stm32f10x_cl` (the directory depends on the target microcontroller)
* from Eclipse, build the `stm32f10x_cl` configuration of the Switch project

#### Modules and Loader
Before building the modules and Loader, you must select their configuration. It has nothing to do with
build configurations in Eclipse, which are only used by the C indexer in Eclipse.

To configure the project, run `make config CONF_BUILD=ab2f107 CONF_MODULEID=0x01` in the project directory.
The first variable specifies the configuration directory, the second variable is the node id used in CAN communication.
The selected configuration is stored in `local.mk`.
Subsequent calls to `make all` will use this configuration. `make clean` will not delete the local configuration.
You can run `make config` with different option if you want to change them.
You can call `make distclean` to delete the local configuration.

Modules can define additional configuration variables. Usually, should not define new variables and only use
`CONF_BUILD` to select a directory where the details are kept together.

You can use Python like `python -mfw build_project TestModule ab2f107 0x01` to build the modules or Loader.
Add `compiler = "make"` to `Tools/fw/local.cfg`.

### Loading

First, you should load the loader using a programmer. Then you can load the applications
using a programmer or the loader. You can then also update the loader (if you want to change the module id, ...) and
programmer is only necessary for debugging or when the loader is corrupted. If you want to upload using SDO objects
(necessary when over CAN), compile the loader with a unique module id.

#### From Eclipse

Select the project which contains the compiled flash image and run *OpenOCD* from *External Tools*.
Then run *Firmware Upload* when uploading an application, or *Loader Upload* when uploading a loader.
The *Debug Upload* is there in case that you needed to disable loader support completely.
After the flash is verified, you can terminate the *OpenOCD* which runs in background.

#### From Python

Use `python -mfw upload_project TestModule` for loading modules, or `python -mfw upload_project Loader loader` for uploading Loader.
The uploader must be selected in `Tools/fw/local.cfg`.

* `uploader = "openocd"` uses OpenOCD and a programmer, the same algorithm as when run from Eclipse
* `uploader = "ihex"` dumps the `ch.hex` to a serial (USB) line. This is quite fast, but does not verify the data (which is usually OK).
  `loader_serial` must be set to the correct serial port in `Tools/fw/local.cfg`.
* `uploader = "slcan"` uses SDO objects and CAN over serial. It does verify the data. In `Tools/fw/local.cfg`, `loader_serial` and `loader_node_id` must be correctly set.

### Running

The microcontroller runs the application after power-up. Only if the application code is definitely corrupted, it can enter
loader mode. When resetting by button, it waits a short time before it starts the application. If you press the button once again,
it will enter loader mode. When uploading using loader, the module can be in loader mode or in application mode (it is reset to loader mode by the python script). 

### Testing

The *Tools* project contains tests for various functionality. Each test may require different firmware, different cable connections, and different settings in `Tools/fw/local.cfg`.

To run a test from eclipse, right-click the file containing the test and choose *Run As* > *Python unit-test*.
To run the test from command line, use like `python -munittest fw.testsystem.serial_basic`.

### Debugging

To debug, specify `USE_DEBUG=yes` (you can add this line to your `local.mk`) and build and upload the module.
You can also use `USE_LOADER=no` to disable loader support. Then the application is not preceded by the loader and this
can be more convenient for debugging.  

## License

This project is licensed under [GNU General Public License](http://www.gnu.org/licenses/gpl.html) version 3 or later.
The license text is in the LICENSE file.

## Authors

### New files

> Robot Firmware - Copyright (C) 2014 Vlastimil Dort

Vlastimil Dort panacekcz AT seznam DOT cz

Some files are

> Robot Firmware - Copyright (C) 2014 Ondřej Holešovský

### Modified files from ChibiOS/RT

This project contains modified files from [ChibiOS/RT](http://chibios.org)

> ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,2011,2012,2013 Giovanni Di Sirio.

Modified files from ChibiOS/RT with GPL license:

* Switch/base.h based on [os/ports/GCC/ARMCMx/crt0.c](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/os/ports/GCC/ARMCMx/crt0.c)
* Switch/cm3.c based on [os/ports/GCC/ARMCMx/STM32F1xx/vectors.c](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/os/ports/GCC/ARMCMx/STM32F1xx/vectors.c)
* ChibiOS/common/chconf.h based on [os/kernel/templates/chconf.h](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/os/kernel/templates/chconf.h)
* Module/stm32f107/loadable.ld based on [os/ports/GCC/ARMCMx/STM32F1xx/ld/STM32F107xC.ld](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/os/ports/GCC/ARMCMx/STM32F1xx/ld/STM32F107xC.ld)
* Loader/stm32f107/loader.ld based on [os/ports/GCC/ARMCMx/STM32F1xx/ld/STM32F107xC.ld](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/os/ports/GCC/ARMCMx/STM32F1xx/ld/STM32F107xC.ld)

Modified files from ChibiOS/RT with Apache license:

* ARMBoard/stm32f107/board.c based on [boards/ST\_STM32VL\_DISCOVERY/board.c](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/boards/ST_STM32VL_DISCOVERY/board.c)
* ARMBoard/stm32f107/board.h based on [boards/ST\_STM32VL\_DISCOVERY/board.h](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/boards/ST_STM32VL_DISCOVERY/board.h)
* ChibiOS/common/halconf.h based on [os/hal/templates/halconf.h](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/os/hal/templates/halconf.h)
* ChibiOS/stm32f107/board.h based on [boards/ST\_STM32VL\_DISCOVERY/board.h](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/boards/ST_STM32VL_DISCOVERY/board.h)
* ChibiOS/stm32f107/mcuconf.h based on [demos/ARMCM3-STM32F107/mcuconf.h](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/demos/ARMCM3-STM32F107/mcuconf.h)
* Communication/commusb.h based on [testhal/STM32F1xx/USB\_CDC\_F107/main.c](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/main.c)
* Communication/commusb.c based on [testhal/STM32F1xx/USB\_CDC\_F107/main.c](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/main.c)
* Communication/commusbcfg.c based on [testhal/STM32F1xx/USB\_CDC\_F107/main.c](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/main.c)
* Communication/commcan.c based on [testhal/STM32F1xx/CAN/main.c](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/CAN/main.c)
* Module/stm32f107/mcuconf.h based on [testhal/STM32F1xx/USB\_CDC\_F107/mcuconf.h](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/mcuconf.h)
* Module/chconf.h based on [testhal/STM32F1xx/USB\_CDC\_F107/chconf.h](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/chconf.h)
* Module/halconf.h based on [testhal/STM32F1xx/USB\_CDC\_F107/halconf.h](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/halconf.h)
* Loader/stm32f107/mcuconf.h based on [testhal/STM32F1xx/USB\_CDC\_F107/mcuconf.h](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/mcuconf.h)
* Loader/chconf.h based on [testhal/STM32F1xx/USB\_CDC\_F107/chconf.h](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/chconf.h)
* Loader/halconf.h based on [testhal/STM32F1xx/USB\_CDC\_F107/halconf.h](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/halconf.h)

Modified files from ChibiOS/RT without license notice:

* Switch/switch.mk based on [os/ports/GCC/ARMCMx/rules.mk](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/os/ports/GCC/ARMCMx/rules.mk)
* ARMBoard/stm32f107/board.mk based on [boards/ST\_STM32VL\_DISCOVERY/board.mk](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/boards/ST_STM32VL_DISCOVERY/board.mk)
* Module/ab2f107/build.mk based on [testhal/STM32F1xx/USB\_CDC\_F107/Makefile](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/Makefile)
* Module/module.mk based on [testhal/STM32F1xx/USB\_CDC\_F107/Makefile](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/Makefile)
* Module/options.mk based on [testhal/STM32F1xx/USB\_CDC\_F107/Makefile](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/Makefile)
* Loader/ab2f107/build.mk based on [testhal/STM32F1xx/USB\_CDC\_F107/Makefile](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/Makefile)
* Loader/Makefile based on [testhal/STM32F1xx/USB\_CDC\_F107/Makefile](http://sourceforge.net/p/chibios/svn/6820/tree/trunk/testhal/STM32F1xx/USB_CDC_F107/Makefile)

### Modified files from openrobots-dev fork of ChibiOS/RT 

This project contains modified files from [openrobots-dev fork of ChibiOS/RT](https://github.com/openrobots-dev/ChibiOS)

Unmodified files from openrobots-dev fork of  ChibiOS/RT with GPL license:

* ChibiOS/thirdparty/qei/qei.h [os/hal/include/qei.h](https://github.com/openrobots-dev/ChibiOS/blob/e8117b7b73b70668343b6328b0c0d2c3c294eb6c/os/hal/include/qei.h)
* ChibiOS/thirdparty/qei/qei\_lld.h [os/hal/platforms/STM32/qei\_lld.h](https://github.com/openrobots-dev/ChibiOS/blob/e8117b7b73b70668343b6328b0c0d2c3c294eb6c/os/hal/platforms/STM32/qei_lld.h)

Modified files from openrobots-dev fork of  ChibiOS/RT with GPL license:

* ChibiOS/thirdparty/qei/qei.c based on [os/hal/src/qei.c](https://github.com/openrobots-dev/ChibiOS/blob/e8117b7b73b70668343b6328b0c0d2c3c294eb6c/os/hal/src/qei.c)
* ChibiOS/thirdparty/qei/qei\_lld.c based on [os/hal/platforms/STM32/qei\_lld.c](https://github.com/openrobots-dev/ChibiOS/blob/e8117b7b73b70668343b6328b0c0d2c3c294eb6c/os/hal/platforms/STM32/qei_lld.c)


### Modified files from OpenOCD

This project contains modified files from [OpenOCD](http://openocd.sourceforge.net/)

Modified files from OpenOCD without license notice:

* ARMBoard/stm32f107/stlink-v2.cfg based on [tcl/board/stm32vldiscovery.cfg](http://sourceforge.net/p/openocd/code/ci/075c3f8fe29c7aecd446c655a7fadb9e1999a31f/tree/tcl/board/stm32vldiscovery.cfg)

### Modified files from bareCortexM

This project contains modified files from [bareCortexM](https://github.com/JorgeAparicio/bareCortexM)

> Copyright (C) 2012 Jorge Aparicio <jorge.aparicio.r@gmail.com>

Modified files from bareCortexM with GPL license:

* Module/stm32f107/upload.script based on [gdb/stm32f107vc.script](https://github.com/JorgeAparicio/bareCortexM/blob/0cf90b9beab7abccc481600d69c6c4f8a3ea77e2/gdb/stm32f107vc.script)
* Module/stm32f107/upload-debug.script based on [gdb/stm32f107vc.script](https://github.com/JorgeAparicio/bareCortexM/blob/0cf90b9beab7abccc481600d69c6c4f8a3ea77e2/gdb/stm32f107vc.script)
* Loader/stm32f107/upload.script based on [gdb/stm32f107vc.script](https://github.com/JorgeAparicio/bareCortexM/blob/0cf90b9beab7abccc481600d69c6c4f8a3ea77e2/gdb/stm32f107vc.script)
