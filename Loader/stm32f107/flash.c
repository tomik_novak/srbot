/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ch.h"
#include "flash.h"
#include "stm32f10x.h"

uint8_t ldr_flash_erased_pages[128];

void ldrFlashUnflagErrors(void){
	FLASH->SR = FLASH_SR_PGERR | FLASH_SR_WRPRTERR;
}

bool_t ldrFlashIsProtectError(void){
	return FLASH->SR & FLASH_SR_WRPRTERR;
}
bool_t ldrFlashIsWriteError(void){
	return FLASH->SR & FLASH_SR_PGERR;
}

void ldrFlashUnlock(void){
	if(FLASH->CR & FLASH_CR_LOCK){
		FLASH->KEYR = 0x45670123;
		FLASH->KEYR = 0xCDEF89AB;
	}
}

void ldrFlashLock(void){
	FLASH->CR = FLASH_CR_LOCK;
}

void ldrFlashEndOperation(void){
	size_t i;
	for(i = 0; i < sizeof(ldr_flash_erased_pages); ++i)
		ldr_flash_erased_pages[i]=0;
}

void ldrFlashErase(uint32_t address){
	uint32_t page = (address >> 11) & 0x7F;
	if(!ldr_flash_erased_pages[page]){
		FLASH->CR = FLASH_CR_PER;
		FLASH->AR = page << 11;
		FLASH->CR = FLASH_CR_STRT | FLASH_CR_PER;
		ldr_flash_erased_pages[page] = 1;
		while(FLASH->SR & FLASH_SR_BSY);
	}
}

void ldrFlashWrite(uint32_t address, uint16_t data){
	FLASH->CR = FLASH_CR_PG;
	*(volatile uint16_t*)address = data;
	while(FLASH->SR & FLASH_SR_BSY);
}
