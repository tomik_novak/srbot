# Robot Firmware - Modified 2014 by Vlastimil Dort
# Robot Firmware - Modified 2014 by Tomas Novak

# Imported source files and paths
include $(WORKSPACE)/ARMBoard/srbotf107/board.mk
include $(CHIBIOS)/os/hal/platforms/STM32F1xx/platform_f105_f107.mk
include $(CHIBIOS)/os/hal/hal.mk
include $(CHIBIOS)/os/ports/GCC/ARMCMx/STM32F1xx/port.mk
include $(CHIBIOS)/os/kernel/kernel.mk
include $(WORKSPACE)/Communication/feature.mk

# Define linker script file here
LDSCRIPT= stm32f107/loader.ld

# C sources that can be compiled in ARM or THUMB mode depending on the global
# setting.
CSRC += stm32f107/flash.c stm32f107/reset.c

#
# Project, sources and paths
##############################################################################


##############################################################################
# Start of user section
#

# List all user C define here, like -D_DEBUG=1
UDEFS = -DCORTEX_VTOR_INIT=0x20000000

# Define ASM defines here
UADEFS =

# List all user directories here
UINCDIR = stm32f107 ab2f107

# List the user directory to look for the libraries here
ULIBDIR = 

# List all user libraries here
ULIBS = $(WORKSPACE)/Switch/stm32f10x_cl/build/switch.o

#
# End of user defines
##############################################################################
