/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ch.h"
#include "ihex.h"
#include "flash.h"
#include "loader.h"
#include "commtext.h"

/// Buffer for decoded IHEX data.
static uint8_t ldr_ihex_content[256];

void ldrIhexCommand(const CommSerial* serial){
	uint8_t ihexl = 0;
	uint8_t checksum = 0;
	BaseSequentialStream* ss = (BaseSequentialStream*)serial->channel;

	const uint8_t* p = serial->line->data + 1;
	for(;p[0] && p[1]; p += 2){
		uint8_t d1 = commTextFromHexDigit(p[0]);
		uint8_t d0 = commTextFromHexDigit(p[1]);
		if(d1 == 255 || d0 == 255){
			printsln(ss, "xHXC");
			return;
		}
		checksum += ldr_ihex_content[ihexl] = d1 << 4 | d0;
		++ihexl;
	}
	// If there is odd number of chars, check the last one to produce the correct error code
	if(p[0] && commTextFromHexDigit(p[0])==255){
		printsln(ss, "xHXC");
		return;
	}

	if(p[0] || ihexl != ldr_ihex_content[0] + 5){
		printsln(ss, "xHXL");
		return;
	}

	if(checksum){
		printsln(ss, "xHXS");
		return;
	}

	switch(ldr_ihex_content[3]){
	case 0x04:
		// Upper 2 bytes of the address
		loader_addresses.current = ldr_ihex_content[4] << 24 | ldr_ihex_content[5] << 16;
		break;
	case 0x00:
		// Data
		ldrFlashUnlock();
		uint32_t rowaddr = loader_addresses.current | ldr_ihex_content[1] << 8 | ldr_ihex_content[2] << 0;
		uint8_t rowlen = ldr_ihex_content[0];
		if(rowaddr % 2){
			printsln(ss, "xHXA");
			return;
		}

		if(!ldrCanWriteRange(rowaddr, rowaddr + rowlen)){
			printsln(ss, "xFSE");
			return;
		}
		uint8_t i;
		palSetPad(GPIOC, GPIOC_LED13);
		for(i = 0; i < rowlen; i += 2){

			uint32_t addr = rowaddr + i;
			
			ldrFlashErase(addr);
			if(i + 1 < rowlen)
				ldrFlashWrite(addr, ldr_ihex_content[5 + i] << 8 | ldr_ihex_content[4 + i]);
			else
				ldrFlashWrite(addr, 0xFF00 | ldr_ihex_content[4 + i]);
		}
		palClearPad(GPIOC, GPIOC_LED13);

		if(ldrFlashIsWriteError())
			printsln(ss, "xFWR");
		if(ldrFlashIsProtectError())
			printsln(ss, "xFPR");
		

		ldrFlashUnflagErrors();
		break;
	case 0x01:
		// End of file
		ldrFlashEndOperation();
		printsln(ss, "aHXE");
		break;
	case 0x05:
		// Ignored
		break;
	default:
		prints(ss, "xHXT");
		printx2(ss, ldr_ihex_content[3]);
		println(ss);
	}
}
