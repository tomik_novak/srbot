/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOADER_H_
#define LOADER_H_

#include "ch.h"

extern uint32_t __app_start__[], __app_end__[], __flash_start__[], __flash_end__[], __ram_start__[], __ram_end__[];

typedef struct {
	uint32_t current;
	uint32_t low;
	uint32_t high;
} LoaderAddresses;

extern LoaderAddresses loader_addresses;

extern uint16_t reset_reason;

/// Reset the microcontroller while storing the reason in the backup memory.
/// @param reason Reason code that will be stored in the backup memory.
void ldrResetWithReason(uint16_t reason);
/// Check whether the loader is allowed to access the address.
bool_t ldrCanWriteRange(uint32_t begin, uint32_t end);


#if !defined(TEST_TOKEN) || defined(__DOXYGEN__)
/// Test token.
/// Override it to non-zero to enable test token.
#define TEST_TOKEN 0
#endif


#endif /* LOADER_H_ */
