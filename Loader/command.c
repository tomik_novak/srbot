/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "command.h"
#include "loader.h"
#include "switch.h"
#include "commtext.h"
#include "flash.h"
#include <string.h>

enum LdrSdoCommand{
	LDR_SDO_COMMAND_END_FILE = 0x00000001,
	LDR_SDO_COMMAND_RANGE_APP = 0x00000011,
	LDR_SDO_COMMAND_RANGE_LOADER = 0x00000012,
};

static void ldrSetRange(void* low, void* high){
	loader_addresses.low = (uint32_t)low;
	loader_addresses.high = (uint32_t)high;
}


void ldrSdoCommand(CommHandlerEvent evt){
	//Command is 32-bit number
	//Write-only, 4 bytes
	switch(evt){
	case COMM_SDO_WRITE_START:
		if(comm_sdo.size != 4)
			comm_sdo.error = COMM_SDO_ERROR_TYPE;
		break;
	case COMM_SDO_WRITE_DATA:
		switch(comm_sdo.data32){
		case LDR_SDO_COMMAND_END_FILE:
			ldrFlashEndOperation();
			break;
		case LDR_SDO_COMMAND_RANGE_APP:
			ldrSetRange(__app_start__, __app_end__);
			break;
		case LDR_SDO_COMMAND_RANGE_LOADER:
			ldrSetRange(__flash_start__, __flash_end__);
			break;
		default:
			comm_sdo.error = COMM_SDO_ERROR_RANGE;
			break;
		}
		break;
	case COMM_SDO_WRITE_BREAK:
	case COMM_SDO_WRITE_END:
		break;
	default:
		comm_sdo.error = COMM_SDO_ERROR_WRITEONLY;
		break;
	}
}

void ldrSdoAddress(CommHandlerEvent evt){
	//Current address and address range (3 32-bit unsigned)
	//Read-write
	//Subindex 0-3
	switch(evt){
	case COMM_SDO_READ_START:
		if(comm_sdo.object_subindex == 0)
			comm_sdo.size = sizeof(loader_addresses);
		else if(comm_sdo.object_subindex <= 3)
			comm_sdo.size = 4;
		else
			comm_sdo.error = COMM_SDO_ERROR_SUBINDEX;
		break;
	case COMM_SDO_READ_DATA:{
		uint32_t object_offset = (comm_sdo.object_subindex == 0 ? 0 : 4 * (comm_sdo.object_subindex - 1));
		commCopy(comm_sdo.data8, ((uint8_t*)&loader_addresses) + object_offset + comm_sdo.index, comm_sdo.segment);
		break;
	}

	case COMM_SDO_WRITE_START:
		if(comm_sdo.object_subindex == 0){
			if(comm_sdo.size != sizeof(loader_addresses))
				comm_sdo.error = COMM_SDO_ERROR_TYPE;
		}
		else if(comm_sdo.object_subindex <= 3){
			if(comm_sdo.size != 4)
				comm_sdo.error = COMM_SDO_ERROR_TYPE;
		}
		else
			comm_sdo.error = COMM_SDO_ERROR_SUBINDEX;
		break;
	case COMM_SDO_WRITE_DATA:{
		uint32_t object_offset = (comm_sdo.object_subindex == 0 ? 0 : 4 * (comm_sdo.object_subindex - 1));
		commCopy(((uint8_t*)&loader_addresses) + object_offset + comm_sdo.index, comm_sdo.data8, comm_sdo.segment);
		break;
	}
	default:
		break;
	}
}

void ldrSdoWrite(uint32_t address, uint8_t low, uint8_t high){
	ldrFlashErase(address);
	ldrFlashWrite(address, high << 8 | low);

	if(ldrFlashIsProtectError() || ldrFlashIsWriteError())
		comm_sdo.error = COMM_SDO_ERROR_DATA_STORE;
}

void ldrSdoData(CommHandlerEvent evt){
	static uint8_t low_byte_buf;

	// Allows writing any number of bytes
	// Reads the whole memory in the address range

	switch(evt){
	case COMM_SDO_READ_START:
		// The object size is the selected section
		comm_sdo.size = loader_addresses.high - loader_addresses.low;
		break;
	case COMM_SDO_READ_DATA:
		commCopy(comm_sdo.data8, (uint8_t*) loader_addresses.low + comm_sdo.index, comm_sdo.segment);
		break;
	case COMM_SDO_WRITE_DATA:{
		uint32_t i = 0;
		if((loader_addresses.current + comm_sdo.index) & 1){
			ldrSdoWrite((loader_addresses.current + comm_sdo.index) - 1, low_byte_buf, comm_sdo.data8[0]);
			i = 1;
		}
		for(;i + 2 <= comm_sdo.segment; i += 2)
			ldrSdoWrite(loader_addresses.current + comm_sdo.index + i, comm_sdo.data8[i], comm_sdo.data8[i + 1]);
		if(i + 1 <= comm_sdo.segment)
			low_byte_buf = comm_sdo.data8[i];
	}
		break;
	case COMM_SDO_WRITE_START:
		if(!ldrCanWriteRange(loader_addresses.current,loader_addresses.current + comm_sdo.size))
			comm_sdo.error = COMM_SDO_ERROR_RANGE;
		else{
			ldrFlashUnlock();
			low_byte_buf = 0xFF;
		}
		break;
	case COMM_SDO_WRITE_END:
		if((loader_addresses.current + comm_sdo.size) & 1){
			ldrSdoWrite((loader_addresses.current + comm_sdo.size) - 1, low_byte_buf, 0xFF);
		}
		// no break
	case COMM_SDO_WRITE_BREAK:
		ldrFlashLock();
		break;
	default:
		break;
	}


}

void ldrSdoReset(CommHandlerEvent event){
	switch(event){
	case COMM_SDO_WRITE_START:
		if(comm_sdo.size != 2)
			comm_sdo.error = COMM_SDO_ERROR_TYPE;
		break;
	case COMM_SDO_WRITE_DATA:
		reset_reason = comm_sdo.data16[0];
		break;
	case COMM_SDO_READ_START:
		comm_sdo.size = 2;
		break;
	case COMM_SDO_READ_DATA:
		comm_sdo.data16[0] = reset_reason;
		break;
	default:
		break;
	}
}

#if TEST_TOKEN
void ldrTestTokenHandler(CommHandlerEvent event){
	switch(event){
	case COMM_SDO_READ_START:
		comm_sdo.size = 4;
		break;
	case COMM_SDO_READ_DATA:
		comm_sdo.data32 = TEST_TOKEN;
	case COMM_SDO_READ_END:
	case COMM_SDO_READ_BREAK:
		break;
	default:
		comm_sdo.error = COMM_SDO_ERROR_READONLY;
		break;
	}
}
#endif


void ldrSerialCommand(const CommSerial* serial){
	BaseSequentialStream* ss = (BaseSequentialStream*)serial->channel;

	switch(serial->line->data[1]){
	case 't':
		prints(ss, "aLOA");
		printx2(ss, COMM_MODULE_ID);
		println(ss);
		break;
#if TEST_TOKEN
	case 'T':
		prints(ss, "a");
		printx8(ss, TEST_TOKEN);
		println(ss);
		break;
#endif
	case 's':
		prints(ss, "a");
		printx4(ss,reset_reason);
		println(ss);
		break;
	case 'v':
		reset_reason = SWITCH_REASON_VERIFICATION_REQUEST;
		commNmtChangeState(COMM_NMT_RESET);
		break;
	case 'a':
		reset_reason = SWITCH_REASON_APPLICATION_REQUEST;
		commNmtChangeState(COMM_NMT_RESET);
		break;
	case 'l':
		reset_reason = SWITCH_REASON_LOADER_REQUEST;
		commNmtChangeState(COMM_NMT_RESET);
		break;
	case 'c':
		prints(ss, "a");
		printx8(ss, loader_addresses.current);
		prints(ss, ";");
		printx8(ss, loader_addresses.low);
		prints(ss, "-");
		printx8(ss, loader_addresses.high);
		println(ss);
		break;
	case 'A':
		ldrSetRange(__app_start__, __app_end__);
		break;
	case 'L':
		ldrSetRange(__flash_start__, __flash_end__);
		break;
	case 'R':
		ldrSetRange(__ram_start__, __ram_end__);
		break;
	case 'F':
		ldrSetRange(__flash_start__, __app_end__);
		break;
	case 'E':
		ldrSetRange(NULL, NULL);
		break;
	default:
		printsln(ss, "xCMD");
		break;
	}
}
