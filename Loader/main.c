/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ch.h"
#include "hal.h"
#include "switch.h"
#include "communication.h"
#include "commserial.h"
#include "commcan.h"
#include "commtext.h"
#include "flash.h"
#include "ihex.h"
#include "command.h"
#include "loader.h"
//#include "armboard.h"

uint16_t reset_reason;

LoaderAddresses loader_addresses = {
	.current = (uint32_t)__app_start__,
	.low = (uint32_t)__app_start__,
	.high = (uint32_t)__app_end__,
};

const CommHandlerEntry comm_pdo_handlers[] = {
		{0x0381, &commEnableOutputHandler, 0},
};
const uint32_t comm_pdo_handler_count = 1;

const CommHandlerEntry comm_sdo_handlers[] = {
		{0x3E01, ldrSdoReset, 0},
		{0x3E02, ldrSdoData, 0},
		{0x3E03, ldrSdoAddress, 0},
		{0x3E04, ldrSdoCommand, 0},
#if TEST_TOKEN
		{0x3E05, ldrTestTokenHandler, 0},
#endif
};
const uint32_t comm_sdo_handler_count = sizeof(comm_sdo_handlers) / sizeof(*comm_sdo_handlers);

bool_t ldrCanWriteRange(uint32_t begin, uint32_t end){
	return loader_addresses.low <= begin && ((begin <= end && (end <= loader_addresses.high || loader_addresses.high == 0)) || (end == 0 && loader_addresses.high == 0));
}

void commTerminated(){
	ldrResetWithReason(reset_reason);
}
bool_t commNmtStateChanging(uint16_t o, uint16_t n){
	(void)o;
	(void)n;
	return true;
}
void commSerialUserCommand(const CommSerial* serial){
	BaseSequentialStream* ss = (BaseSequentialStream*) serial->channel;

	if(serial->line->data[0] == 'l'){
		ldrSerialCommand(serial);
	}
	else if(serial->line->data[0] == ':'){
		ldrIhexCommand(serial);
	}
	else if(serial->line->length){
		printsln(ss, "CTY");
	}
}

int main(void) {

	PWR->CR |= PWR_CR_DBP;
	// Read reset reason before ChibiOS clears it
	reset_reason = BKP->DR41;
	// Init chibios
	halInit();
	chSysInit();
	// Light up the led to indicate USB does not work yet
	palSetPad(GPIOC, GPIOC_LED14);

	// Start communication
	commStartAll();

	for(;;){
				
		palWritePad(GPIOC, GPIOC_LED14, !commUsbIsConnected());
		chThdSleepMilliseconds(100);
	}
}

