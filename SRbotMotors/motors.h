/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef	MOTORS_H_
#define MOTORS_H_



#include "module.h"
#include "ch.h"

#if !defined(MOTORS_PWM_TIMER_FREQ)
/// Frequency of the PWM timers.
#define MOTORS_PWM_TIMER_FREQ 10000 //TODO: usable default
#endif

#if !defined(MOTORS_PWM_PERIOD)
/// Period of the PWM timers, in ticks specified by the timer frequency.
#define MOTORS_PWM_PERIOD 100 //TODO: usable default
#endif

typedef struct{
	PWMDriver* pwm;
	uint8_t pwm_ch1, pwm_ch2;
}Motor;

/// Set the motor speed and direction.
void motorSet(const Motor* motor, int16_t value);
void motorSetI(const Motor* motor, int16_t value);
/// Set the motor forward speed.
void motorForwardI(const Motor* motor, uint16_t value);
/// Set the motor backward speed.
void motorBackwardI(const Motor* motor, uint16_t value);
/// Set the motor to free state.
void motorFree(const Motor* motor);
void motorFreeI(const Motor* motor);
/// Set the motor to brake state.
void motorBrake(const Motor* motor);
void motorBrakeI(const Motor* motor);

extern const Motor motor_left;
extern const Motor motor_right;

/// Enable motor control.
void motorsStart(void);

#endif
