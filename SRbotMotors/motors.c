/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is modified from Robot Firmware, modified by Tomas Novak.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "motors.h"
#include "hal.h"


/// Configuration for top PWMD3.
static const PWMConfig pwm_config = {
	MOTORS_PWM_TIMER_FREQ,
	MOTORS_PWM_PERIOD,
	NULL,
	{
		{PWM_OUTPUT_ACTIVE_HIGH, NULL},//TIM3_CH1 on PC6
		{PWM_OUTPUT_ACTIVE_HIGH, NULL},//TIM3_CH2 on PC7
		{PWM_OUTPUT_ACTIVE_HIGH, NULL},//TIM3_CH3 on PC8
		{PWM_OUTPUT_ACTIVE_HIGH, NULL} //TIM3_CH4 on PC9
	},
	0,
	0,
};

const Motor motor_left = {
	&PWMD3, 1, 0
};

const Motor motor_right = {
	&PWMD3, 3, 2
};

void motorSet(const Motor* motor, int16_t value){
	chSysLock();
	motorSetI(motor, value);
	chSysUnlock();
}
void motorFree(const Motor* motor){
	chSysLock();
	motorFreeI(motor);
	chSysUnlock();
}
void motorBrake(const Motor* motor){
	chSysLock();
	motorBrakeI(motor);
	chSysUnlock();
}

void motorSetI(const Motor* motor, int16_t value){
	if(value > 0)
		motorForwardI(motor, value);
	else if(value < 0)
		motorBackwardI(motor, -value);
	else
		motorFreeI(motor);
}
void motorForwardI(const Motor* motor, uint16_t value){
	if(value >= MOTORS_PWM_PERIOD)
		value = MOTORS_PWM_PERIOD-1; //TODO: check this

	pwmEnableChannelI(motor->pwm, motor->pwm_ch1,  value);
	pwmEnableChannelI(motor->pwm, motor->pwm_ch2, 0);
}
void motorBackwardI(const Motor* motor, uint16_t value){
	if(value >= MOTORS_PWM_PERIOD)
		value = MOTORS_PWM_PERIOD-1; //TODO: check this

	pwmEnableChannelI(motor->pwm, motor->pwm_ch1, 0);
	pwmEnableChannelI(motor->pwm, motor->pwm_ch2, value);
}
void motorFreeI(const Motor* motor){
	pwmEnableChannelI(motor->pwm, motor->pwm_ch1, 0);
	pwmEnableChannelI(motor->pwm, motor->pwm_ch2, 0);
}
void motorBrakeI(const Motor* motor){
	uint16_t max = MOTORS_PWM_PERIOD-1;
	pwmEnableChannelI(motor->pwm, motor->pwm_ch1, max);
	pwmEnableChannelI(motor->pwm, motor->pwm_ch2, max);
}


void motorsStart(void){
	pwmStart(motor_left.pwm, &pwm_config);
	
}
