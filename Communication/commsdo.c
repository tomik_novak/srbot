/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "communication.h"
#include "commusb.h"

typedef enum {
	COMM_SDO_IDLE,
	COMM_SDO_W0,
	COMM_SDO_W1,
	COMM_SDO_R0,
	COMM_SDO_R1,
	COMM_SDO_ERROR,
} CommSdoState;

static CommSdoState comm_sdo_state = COMM_SDO_IDLE;

#if COMM_SDO_TIMEOUT != 0 || defined(__DOXYGEN__)
/// Timer used to break SDO session.
static VirtualTimer comm_sdo_timer;
/// Indicates that timer elapsed and the session should be broken.
static bool_t comm_sdo_timeout;
/// Used to notify the communication thread.
EventSource comm_sdo_timer_event;
#endif

CommSdo comm_sdo;
const CommHandlerEntry* comm_sdo_handler = NULL;

#if COMM_DIAGNOSTICS != 0 || defined(__DOXYGEN__)
uint32_t comm_sdo_errors = 0;
#endif

const CommHandlerEntry* commFindSdoHandler(uint16_t objIndex){
	uint32_t i;
	for(i=0; i<comm_sdo_handler_count; ++i)
		if(comm_sdo_handlers[i].id == objIndex)
			return comm_sdo_handlers+i;
	return NULL;
}

/// Get command type from current frame.
/// SDO COMMAND : 3 bits type, 1 bit toggle
static inline uint8_t commSdoCommandType(void){
	return (comm_frame.data8[0] >> 4) & 0x0F;
}
/// Get number of valid bytes for expedited write from current frame.
static inline uint8_t commSdoCommandExpeditedSize(void){
	return 4 - ((comm_frame.data8[0] >> 2) & 0x03);
}

/// Check whether the operation that is initiated by the current frame
/// is expedited or segmented.
/// @return 1 if expedited, 0 if segmented.
static inline bool_t commSdoCommandIsExpedited(void){
	return (comm_frame.data8[0] >> 1) & 0x01;
}
/// Check whether the current SDO frame is the last one.
static inline bool_t commSdoCommandIsEnd(void){
	return comm_frame.data8[0] & 0x01;
}

/// Get number of valid bytes for segmented write from current frame.
static inline uint8_t commSdoCommandSegmentedSize(void){
	return 7 - ((comm_frame.data8[0] >> 1) & 0x07);
}

/// Get the SDO command id for a reply to expedited read,
/// based on the size of the data.
static inline uint8_t commSdoReplyExpeditedRead(uint8_t size){
	return 0x43 | (4-size)<<2;
}

/// Get the SDO command id for a end reply to segmented read
/// based on the current state and the size of the last part of the data.
static inline uint8_t commSdoReplySegmentedReadEnd(uint8_t size){
	return (comm_sdo_state==COMM_SDO_R0 ? 0x01 : 0x11) | (7-size)<<1;
}

/// Get the SDO command id for a reply to segmented read
/// based on the current state.
static inline uint8_t commSdoReplySegmentedRead(void){
	return (comm_sdo_state==COMM_SDO_R0 ? 0x00 : 0x10);
}

/// Dispatch (send to the master) the current frame in ::comm_frame
/// with the correct length and sid for a SDO reply.
static void commDispatchSdo(void){
	comm_frame.length = 8;
	comm_frame.sid = 0x580 + COMM_MODULE_ID;
	commDispatchFrame(COMM_SOURCE_MODULE);
}

/// Dispatch a frame which confirms a segmented write request, or an expedited write.
static void commSdoConfirmWriteStart(void){
	// The object index and subindex are already present in the frame
	comm_frame.data8[0] = 0x60;
	comm_frame.data32[1] = 0;

	commDispatchSdo();
}

static void commSdoConfirmWrite(void){
	// The object index and subindex are not sent
	comm_frame.data32[0] = 0;
	comm_frame.data32[1] = 0;
	comm_frame.data8[0] = comm_sdo_state == COMM_SDO_W0 ? 0x20 : 0x30;

	commDispatchSdo();
}


static void commSdoSendError(void){
#if COMM_DIAGNOSTICS
	comm_sdo_errors++;
#endif
	// The error code is in comm_sdo_error
	comm_frame.data8[0] = 0x80;
	comm_frame.data8[1] = comm_sdo.object_index; //LSB
	comm_frame.data8[2] = comm_sdo.object_index >> 8; //MSB
	comm_frame.data8[3] = comm_sdo.object_subindex;
	comm_frame.data32[1] = comm_sdo.error;

	commDispatchSdo();
}

#if COMM_SDO_TIMEOUT != 0
static void commSdoTimerElapsed(void* param){
	(void)param;
	chSysLockFromIsr();
	comm_sdo_timeout = true;
	chEvtBroadcastFlagsI(&comm_sdo_timer_event,0);
	chSysUnlockFromIsr();
}
#endif

static void commSdoGoToState(CommSdoState state){
	comm_sdo_state = state;
	if(state != COMM_SDO_IDLE){
#if COMM_SDO_TIMEOUT != 0
		chVTSet(&comm_sdo_timer, MS2ST(COMM_SDO_TIMEOUT), &commSdoTimerElapsed, NULL);
#endif
	}
}

static void commSdoReceiveSize(void){
	// Prepare values for handler
	comm_sdo.size = comm_frame.data32[1];
	comm_sdo.index = 0;
	comm_sdo.error = 0;
	comm_sdo_handler->handler(COMM_SDO_WRITE_START);
	if(comm_sdo.error == 0){
		commSdoConfirmWriteStart();
		// Go to write state and start timeout
		commSdoGoToState(COMM_SDO_W0);
	}
	else{
		comm_sdo_handler = NULL;
		commSdoSendError();
		// State is idle
	}
}

static void commSdoReceive(void){
	// Prepare values for handler
	comm_sdo.error = 0;
	comm_sdo.segment = 7; //Always 7 bytes

	commCopy(comm_sdo.data8, comm_frame.data8+1, 7);

	comm_sdo_handler->handler(COMM_SDO_WRITE_DATA);
	if(comm_sdo.error == 0){
		comm_sdo.index+=7;
		commSdoConfirmWrite();
		commSdoGoToState(comm_sdo_state==COMM_SDO_W0 ? COMM_SDO_W1 : COMM_SDO_W0);
	}
	else{
		// Process error and return to idle
		comm_sdo_handler->handler(COMM_SDO_WRITE_BREAK);
		comm_sdo_handler = NULL;
		commSdoSendError();
		commSdoGoToState(COMM_SDO_IDLE);
	}
}

static void commSdoReceiveEnd(void){

	uint8_t segment = commSdoCommandSegmentedSize();
	// Prepare values for handler
	comm_sdo.error = 0;
	comm_sdo.segment = segment;
	commCopy(comm_sdo.data8, comm_frame.data8 + 1, segment);

	comm_sdo_handler->handler(COMM_SDO_WRITE_DATA);
	if(comm_sdo.error == 0){
		commSdoConfirmWrite();
		comm_sdo_handler->handler(COMM_SDO_WRITE_END);
	}
	else{
		// Process error and return to idle
		comm_sdo_handler->handler(COMM_SDO_WRITE_BREAK);
		commSdoSendError();
	}
	comm_sdo_handler = NULL;
	commSdoGoToState(COMM_SDO_IDLE);
}


static void commSdoReceiveExpedited(void){
	// The size is indicated in the command
	uint8_t size = commSdoCommandExpeditedSize();

	// Prepare values for the handler
	comm_sdo.size = size;
	comm_sdo.segment = size;
	comm_sdo.index = 0;
	comm_sdo.error = 0;
	comm_sdo.data32 = comm_frame.data32[1];

	// Execute all handlers
	comm_sdo_handler->handler(COMM_SDO_WRITE_START);
	if(comm_sdo.error == 0){
		comm_sdo_handler->handler(COMM_SDO_WRITE_DATA);
		if(comm_sdo.error == 0){
			comm_sdo_handler->handler(COMM_SDO_WRITE_END);
		}
		else{
			comm_sdo_handler->handler(COMM_SDO_WRITE_BREAK);
		}
	}
	comm_sdo_handler = NULL;

	if(comm_sdo.error == 0)
		commSdoConfirmWriteStart();
	else
		commSdoSendError();

	// State is idle
}


static void commSdoTransmitSize(void){
	//Object index and subindex already filled
	comm_frame.data8[0] = 0x41;
	comm_frame.data32[1] = comm_sdo.size;
	commDispatchSdo();

	commSdoGoToState(COMM_SDO_R0);
}

static void commSdoTransmitEnd(void){
	// Index and size already filled
	uint8_t segment = comm_sdo.size - comm_sdo.index;
	comm_sdo.segment = segment;
	comm_sdo.error = 0;
	comm_sdo_handler->handler(COMM_SDO_READ_DATA);
	if(comm_sdo.error == 0){
		comm_sdo_handler->handler(COMM_SDO_READ_END);

		comm_frame.data8[0] = commSdoReplySegmentedReadEnd(segment);
		//Copy the data
		commCopy(comm_frame.data8 + 1, comm_sdo.data8, segment);

		uint8_t i;
		//Clear unused bytes
		for(i = segment; i<7;++i)
			comm_frame.data8[i+1]=0;
		commDispatchSdo();
	}
	else{
		comm_sdo_handler->handler(COMM_SDO_READ_BREAK);
		commSdoSendError();
	}

	comm_sdo_handler = NULL;
	commSdoGoToState(COMM_SDO_IDLE);
}

static void commSdoTransmit(void){
	// Index and size already filled
	// Here we always read 7 bytes
	comm_sdo.segment = 7;
	comm_sdo.error = 0;
	comm_sdo_handler->handler(COMM_SDO_READ_DATA);

	if(comm_sdo.error == 0){
		comm_sdo.index+=7;

		comm_frame.data8[0] = commSdoReplySegmentedRead();
		commCopy(comm_frame.data8 + 1, comm_sdo.data8, 7);
		commDispatchSdo();

		commSdoGoToState(comm_sdo_state == COMM_SDO_R0 ? COMM_SDO_R1 : COMM_SDO_R0);
	}
	else{
		comm_sdo_handler->handler(COMM_SDO_READ_BREAK);
		comm_sdo_handler = NULL;
		commSdoSendError();
		commSdoGoToState(COMM_SDO_IDLE);
	}
}



static void commSdoTransmitExpedited(void){
	// Prepare values for the handler
	// index, size and error are already filled
	comm_sdo.segment = comm_sdo.size;
	// Get data from the handler
	comm_sdo.data32 = 0;
	comm_sdo_handler->handler(COMM_SDO_READ_DATA);
	if(comm_sdo.error == 0){
		comm_sdo_handler->handler(COMM_SDO_READ_END);
		// Object index and subindex already filled
		comm_frame.data32[1] = comm_sdo.data32;
		comm_frame.data8[0] = commSdoReplyExpeditedRead(comm_sdo.size);
		commDispatchSdo();
	}
	else{
		comm_sdo_handler->handler(COMM_SDO_READ_BREAK);
		commSdoSendError();
	}
	comm_sdo_handler = NULL;
	//State is idle
}


static bool commSdoSessionStart(void){
	comm_sdo.object_index = (uint16_t)comm_frame.data8[2]<<8 | (uint16_t)comm_frame.data8[1];
	comm_sdo.object_subindex = comm_frame.data8[3];
	comm_sdo_handler = commFindSdoHandler(comm_sdo.object_index);

	if(comm_sdo_handler == NULL){
		comm_sdo.error = COMM_SDO_ERROR_NOTEXIST;
		commSdoSendError();
		return false;
	}
	else{
		return true;
	}
}

static void commSdoWriteStart(void){
	if(commSdoSessionStart()){
		if(commSdoCommandIsExpedited())
			commSdoReceiveExpedited();
		else
			commSdoReceiveSize();
	}
}
static void commSdoWrite(void){
	if(commSdoCommandIsEnd())
		commSdoReceiveEnd();
	else
		commSdoReceive();
}


static void commSdoReadStart(void){
	if(commSdoSessionStart()){
		// Prepare values for handler
		comm_sdo.error = 0;

		comm_sdo_handler->handler(COMM_SDO_READ_START);

		if(comm_sdo.error == 0){
			comm_sdo.index = 0;
			if(comm_sdo.size<=4)
				commSdoTransmitExpedited();
			else
				commSdoTransmitSize();
		}
		else{
			comm_sdo_handler = NULL;
			commSdoSendError();
			// State is idle
		}
	}
}

static void commSdoRead(void){
	int remaining = comm_sdo.size - comm_sdo.index;
	if(remaining<=7)
		commSdoTransmitEnd();
	else
		commSdoTransmit();
}


static bool_t commSdoCheckState(CommSdoState expected_state){
	if(comm_sdo_state != expected_state){
		if(comm_sdo_handler != NULL){
			if(comm_sdo_state == COMM_SDO_W0 || comm_sdo_state == COMM_SDO_W1)
				comm_sdo_handler->handler(COMM_SDO_WRITE_BREAK);
			else if(comm_sdo_state == COMM_SDO_R0 || comm_sdo_state == COMM_SDO_R1)
				comm_sdo_handler->handler(COMM_SDO_READ_BREAK);
			comm_sdo_handler = NULL;
		}
		// Do not send error to the master
#if COMM_DIAGNOSTICS
		comm_sdo_errors++;
#endif
		comm_sdo_state = COMM_SDO_IDLE;
		return FALSE;
	}
	return TRUE;
}

#if COMM_SDO_TIMEOUT != 0
void commSdoTimerInit(void){
	chEvtInit(&comm_sdo_timer_event);
}

void commHandleSdoTimerEvent(void){
	bool should_break;
	chSysLock();
	should_break = comm_sdo_timeout;
	comm_sdo_timeout = false;
	chSysUnlock();
	if(should_break){
		comm_sdo.error = COMM_SDO_ERROR_TIMEOUT;
		if(comm_sdo_handler != NULL){
			if(comm_sdo_state == COMM_SDO_W0 || comm_sdo_state == COMM_SDO_W1)
				comm_sdo_handler->handler(COMM_SDO_WRITE_BREAK);
			else if(comm_sdo_state == COMM_SDO_R0 || comm_sdo_state == COMM_SDO_R1)
				comm_sdo_handler->handler(COMM_SDO_READ_BREAK);
			comm_sdo_handler = NULL;
		}
		commSdoSendError();
		commSdoGoToState(COMM_SDO_IDLE);
	}
}
#endif

void commHandleSdoFrame(void){
#if COMM_SDO_TIMEOUT != 0
	chVTReset(&comm_sdo_timer);
	chSysLock();
	comm_sdo_timeout = false;
	chSysUnlock();
#endif

	switch(commSdoCommandType()){
	case 0x2: // Write start
		commSdoCheckState(COMM_SDO_IDLE);
		commSdoWriteStart();
		break;
	case 0x4: // Read start
		commSdoCheckState(COMM_SDO_IDLE);
		commSdoReadStart();
		break;
	case 0x0: // Write 0
		if(commSdoCheckState(COMM_SDO_W0))
			commSdoWrite();
		break;
	case 0x1: // Write 1
		if(commSdoCheckState(COMM_SDO_W1))
			commSdoWrite();
		break;
	case 0x6: // Read 0
		if(commSdoCheckState(COMM_SDO_R0))
			commSdoRead();
		break;
	case 0x7: // Read 1
		if(commSdoCheckState(COMM_SDO_R1))
			commSdoRead();
		break;
	default:
		commSdoCheckState(COMM_SDO_ERROR);
		break;
	}
}
