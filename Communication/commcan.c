/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio
    Robot Firmware - Modified 2014 by Vlastimil Dort

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "communication.h"
#include "commusb.h"
#include "hal.h"
#include "commcan.h"

#if defined(__DOXYGEN__) || COMM_ENABLE_CAN

#if defined(__DOXYGEN__) || COMM_DIAGNOSTICS
uint32_t comm_can_missed_transmissions;

#if defined(__DOXYGEN__) || COMM_CAN_FAST_READER
uint32_t comm_can_missed_frames_fast;
#endif

#endif

#if COMM_ENABLE_FILTERS
void canSTM32SetFilters(uint32_t can2sb, uint32_t num, const CANFilter* cfp);
#endif

/// CAN hardware configuration
static const CANConfig comm_can_config = {
	CAN_MCR_ABOM | CAN_MCR_AWUM | CAN_MCR_TXFP,
#if STM32_PCLK1 == 36000000
	CAN_BTR_SJW(0) | CAN_BTR_TS2(1) | CAN_BTR_TS1(14) | CAN_BTR_BRP(1)
#elif STM32_PCLK1 == 24000000
	CAN_BTR_SJW(0) | CAN_BTR_TS2(0) | CAN_BTR_TS1(5) | CAN_BTR_BRP(2)
#else
#error Frequency not supproted
#endif
};

/// CAN receive frame.
CANRxFrame comm_can_rx_frame;
/// CAN transmit frame.
CANTxFrame comm_can_tx_frame;

void commCanInit(void){
#if COMM_ENABLE_FILTERS
	canSTM32SetFilters(comm_can_filter_count, comm_can_filter_count, comm_can_filters);
#endif
	canStart(&CAND1, &comm_can_config);
}

/// Receives a CAN frame into ::comm_frame, if any.
/// @return TRUE if received, FALSE if not.
static bool_t commCanTryReceive(void){
	if(canReceive(&CAND1, CAN_ANY_MAILBOX, &comm_can_rx_frame, TIME_IMMEDIATE) == RDY_OK){
		comm_frame.data32[0] = comm_can_rx_frame.data32[0];
		comm_frame.data32[1] = comm_can_rx_frame.data32[1];
		comm_frame.sid = comm_can_rx_frame.SID;
		comm_frame.length = comm_can_rx_frame.DLC;
		return TRUE;
	}
	return FALSE;
}

/// Receive and handle CAN frame as long as there are any.
void commCanReceiveAll(void){
	while(commCanTryReceive()){
		commDispatchFrame(COMM_SOURCE_CAN);
	}
}

/// Transmit ::comm_frame via CAN.
void commCanTransmit(void){
	comm_can_tx_frame.IDE = CAN_IDE_STD;
	comm_can_tx_frame.RTR = CAN_RTR_DATA;

	comm_can_tx_frame.DLC = comm_frame.length;
	comm_can_tx_frame.SID = comm_frame.sid;

	comm_can_tx_frame.data32[0] = comm_frame.data32[0];
	comm_can_tx_frame.data32[1] = comm_frame.data32[1];

	msg_t result = canTransmit(&CAND1, CAN_ANY_MAILBOX, &comm_can_tx_frame, MS2ST(100));

#if COMM_DIAGNOSTICS
	if(result != RDY_OK)
		comm_can_missed_transmissions++;
#else
	(void)result;
#endif

}

#if defined(__DOXYGEN__) || COMM_CAN_FAST_READER

static WORKING_AREA(waCommCanFastReader, 128);

static msg_t commCanFastReader(void* arg){
	// The loop can only break if CAN driver is stopped
	while(canReceive(&CAND1, CAN_ANY_MAILBOX, &comm_can_rx_frame, TIME_INFINITE) == RDY_OK){
		CommBufferItem* i = commBufferPushLock();
		if(i != NULL){
			i->frame.data32[0] = comm_can_rx_frame.data32[0];
			i->frame.data32[1] = comm_can_rx_frame.data32[1];
			i->frame.length = comm_can_rx_frame.DLC;
			i->frame.sid = comm_can_rx_frame.SID;
			i->source = COMM_SOURCE_CAN;
			commBufferPushUnlock();
		}
		else{
#if COMM_DIAGNOSTICS
			chSysLock();
			comm_can_missed_frames_fast++;
			chSysUnlock();
#endif
		}
	}
	return 0;
}

Thread* commCanStartFastReader(void){
	return chThdCreateStatic(waCommCanFastReader, sizeof(waCommCanFastReader), HIGHPRIO, commCanFastReader, NULL);
}
#endif

#endif
