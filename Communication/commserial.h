/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMSERIAL_H_
#define COMMSERIAL_H_

#include "communication.h"
#include "hal.h"

#if defined(__DOXYGEN__) || !defined(COMM_SERIAL_LINE_SIZE)
/// USB command line size. Can be overridden.
#define COMM_SERIAL_LINE_SIZE 256
#endif

/// One line received from a serial line.
typedef struct{
	/// String representing the line.
	uint8_t data[COMM_SERIAL_LINE_SIZE];
	/// Length of the line.
	uint16_t length;
}CommSerialLine;

/// Serial line specification.
typedef struct {
	/// Line received from the serial.
	CommSerialLine* line;
	/// Source indicator used in ::commDispatch.
	CommFrameSource source;
	/// Channel of the serial.
	BaseChannel* channel;
}CommSerial;


/// Parse the serial line into a frame (::comm_frame) and dispatch it.
void commSerialFrame(const CommSerial* serial);
/// Execute a command received from the serial line.
void commSerialCommand(const CommSerial* serial);
/// Receive all available characters from the serial line
/// and run ::commSerialCommand on each received line.
void commSerialReceiveAll(const CommSerial* serial);
/// Transmits frame in commFrame over serial
void commSerialTransmit(const CommSerial* serial);

/// This function should be implemented by users to handle
/// specific serial commands.
extern void commSerialUserCommand(const CommSerial* serial);

#endif /* COMMSERIAL_H_ */
