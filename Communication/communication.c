/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "communication.h"
#include "commbuffer.h"
#include "commdrivers.h"
#include "commtext.h"
#include "ch.h"
#include "hal.h"

#if COMM_ENABLE_BUFFER
/// Buffer of frames added by application threads, ISRs or fast readers.
CommBuffer comm_buffer;
#endif

/// Current frame
CommFrame comm_frame;

/// Output mode.
uint32_t comm_output_mode = (1 << COMM_SOURCE_MODULE);

/// Working area for the communication thread.
static WORKING_AREA(wa_comm_thread, 512);

static EventListener listeners[];

static inline uint8_t commIsCurrentPdo(void){
	return comm_frame.sid > 0x180 && comm_frame.sid < 0x580;
}
static inline uint8_t commIsCurrentSdo(void){
	return comm_frame.sid == 0x600 + COMM_MODULE_ID;
}
static inline uint8_t commIsCurrentNmt(void){
	return comm_frame.sid == 0x000 && comm_frame.data8[1] == COMM_MODULE_ID;
}

void commEnableOutputHandler(CommHandlerEvent e){
	comm_output_mode |= comm_frame.data32[0];
}


static void commHandleFrame(void){
	if(commIsCurrentSdo())
		commHandleSdoFrame();
	else if(commIsCurrentPdo())
		commHandlePdoFrame();
	else if(commIsCurrentNmt())
		commHandleNmtFrame();
	//Ignore boot frames and invalid frames
}

void commDispatchFrame(CommFrameSource source){
	// Transmit the frame by all drivers
	// except the source
	for(uint32_t i = 0; i < comm_driver_count; ++i){
		if(comm_drivers[i].transmit && comm_drivers[i].id != source && (comm_output_mode & 1 << comm_drivers[i].id))
			comm_drivers[i].transmit();
	}
}

static void commInit(void){
#if COMM_ENABLE_BUFFER
	// Initialize buffer
	commBufferInit(&comm_buffer);
#endif
	// Initialize drivers
	for(uint32_t i = 0; i < comm_driver_count; ++i){
		if(comm_drivers[i].init)
			comm_drivers[i].init();
	}

}

#if COMM_ENABLE_BUFFER

static void commBufferDispatchAll(void){
	CommBufferItem* cbi;

	while((cbi = commBufferPopLock(&comm_buffer)) != NULL){
		CommFrameSource src = cbi->source;
		comm_frame = cbi->frame;
		commBufferPopUnlock(&comm_buffer);
		commDispatchFrame(src);
	}
}

#endif


static msg_t commThread(void* arg){
	(void)arg;

	// Register event listeners.
	for(uint32_t i = 0; i < comm_driver_count; ++i){
		if(comm_drivers[i].event)
			chEvtRegister(comm_drivers[i].event, &listeners[i], i);
	}

	// Main loop.
	while(comm_nmt_state != COMM_NMT_RESET){
		eventmask_t events = chEvtWaitAny(ALL_EVENTS); // Wait infinitely for some event.

		// This implementation assumes there is low activity so
		// it does not interleave frame sources.

		for(uint32_t i = 0; i < comm_driver_count; ++i){
			if(comm_drivers[i].receive && (events & EVENT_MASK(i)))
				comm_drivers[i].receive();
		}

	}

	for(uint32_t i = 0; i < comm_driver_count; ++i){
		if(comm_drivers[i].fini)
			comm_drivers[i].fini();
	}

	commTerminated();

	return 0;
}

void commCopy(uint8_t* d, const uint8_t* s, int l){
	for(int i=0;i<l;++i)
		d[i]=s[i];
}

Thread* commStart(void){
	commInit(); //Called on this thread to ensure that the caller can start sending messages right away.
	return chThdCreateStatic(wa_comm_thread, sizeof(wa_comm_thread), NORMALPRIO, commThread, NULL);
}

void commStartAll(void){
	commStart();
#if COMM_CAN_FAST_READER
	commCanStartFastReader();
#endif
}

const CommSourceDriver comm_drivers[]={
#if COMM_ENABLE_CAN
#if COMM_CAN_FAST_READER
		{COMM_SOURCE_CAN, &commCanInit, NULL , &commCanTransmit, NULL,  NULL},
#else
		{COMM_SOURCE_CAN, &commCanInit, &commCanReceiveAll, &commCanTransmit, NULL, &CAND1.rxfull_event},
#endif

#endif
#if COMM_ENABLE_USB
		{COMM_SOURCE_USB, &commUsbInit, &commUsbReceiveAll, &commUsbTransmit, NULL, &SDU1.event},
#endif
#if COMM_ENABLE_UART1
		{COMM_SOURCE_UART1, &commUart1Init, &commUart1ReceiveAll, &commUart1Transmit, NULL, &SD1.event},
#endif
#if COMM_SDO_TIMEOUT !=0
		{COMM_SOURCE_NONE,  &commSdoTimerInit, &commHandleSdoTimerEvent, NULL, NULL, &comm_sdo_timer_event},
#endif
#if COMM_HEARTBEAT != 0
		{COMM_SOURCE_NONE,  &commHeartbeatInit, &commHandleHeartbeatEvent, NULL, &commHeartbeatFini, &comm_heartbeat_event},
#endif
		// The frame processing must come at the end
		// because it can modify the frame.
#if COMM_ENABLE_BUFFER
		{COMM_SOURCE_MODULE, NULL, &commBufferDispatchAll, &commHandleFrame, NULL, &comm_buffer.event},
#else
		{COMM_SOURCE_MODULE, NULL, NULL, &commHandleFrame, NULL, NULL},
#endif
};

static EventListener listeners[sizeof(comm_drivers) / sizeof(*comm_drivers)];
const uint32_t comm_driver_count = sizeof(comm_drivers) / sizeof(*comm_drivers);
