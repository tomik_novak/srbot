/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file commbuffer.c
 * Communication buffer.
 * Something like InputQueue or Mailbox, but
 * - the items are not bytes but CommFrames
 * - all operations can be non-blocking
 * - you can post frames from normal or ISR context
 * - data ready is signaled by event
 *
 */
#include "commbuffer.h"

#if COMM_ENABLE_BUFFER

void commBufferInit(CommBuffer* cb){
	cb->begin = cb->end = cb->buffer;
	chSemInit(&cb->empty_sem, COMM_BUFFER_SIZE);
	chEvtInit(&cb->event);
}

CommBufferItem* commBufferPushLockTimeout(CommBuffer* cb, systime_t time){
	chSysLock();
	if(chSemWaitTimeoutS(&cb->empty_sem, time) == RDY_OK)
		return cb->end;
	chSysUnlock();
	return NULL;
}

void commBufferPushUnlock(CommBuffer* cb){
	cb->end++;
	if(cb->end == cb->buffer + COMM_BUFFER_SIZE)
		cb->end = cb->buffer;
	// This function can only be called after commBufferPushLock in locked state,
	// so the value of the semaphore is known to be positive.
	chSysUnlock();
	// Now the consumer can read the frame
	// so it might occur that the event is signaled
	// and there are no data already.
	chEvtBroadcastFlags(&cb->event, 0);
}


CommBufferItem* commBufferPushLockI(CommBuffer* cb){
	if(chSemGetCounterI(&cb->empty_sem) > 0){
		chSemFastWaitI(&cb->empty_sem);
		return cb->end;
	}
	return NULL;
}

void commBufferPushUnlockI(CommBuffer* cb){
	cb->end++;
	if(cb->end == cb->buffer + COMM_BUFFER_SIZE)
		cb->end = cb->buffer;
	chEvtBroadcastFlagsI(&cb->event, 0);
}

CommBufferItem* commBufferPopLock(CommBuffer* cb){
	chSysLock();
	if(chSemGetCounterI(&cb->empty_sem)!=COMM_BUFFER_SIZE)
		return cb->begin;
	chSysUnlock();
	return NULL;
}

void commBufferPopUnlock(CommBuffer* cb){
	cb->begin++;
	if(cb->begin == cb->buffer + COMM_BUFFER_SIZE)
		cb->begin = cb->buffer;
	chSemSignalI(&cb->empty_sem);
	chSchRescheduleS();
	chSysUnlock();
}

void commBufferUnlock(CommBuffer* cb){
	(void)cb;
	chSysUnlock();
}

#endif
