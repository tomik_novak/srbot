/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio
    Robot Firmware - Modified 2014 by Vlastimil Dort

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
#include "commusb.h"
#include "commdrivers.h"
#include "commserial.h"
#include "communication.h"

#if COMM_DEBUG_LEDS
#include "armboard.h"
#endif

SerialUSBDriver SDU1;

static const USBDescriptor vcom_device_descriptor = {
	sizeof vcom_device_descriptor_data,
	vcom_device_descriptor_data
};

/// Configuration Descriptor wrapper.
static const USBDescriptor vcom_configuration_descriptor = {
	sizeof vcom_configuration_descriptor_data,
	vcom_configuration_descriptor_data
};

/// Strings wrappers array.
static const USBDescriptor vcom_strings[] = {
	{sizeof vcom_string0, vcom_string0},
};

/// Handles the GET_DESCRIPTOR callback. All required descriptors must be
/// handled here.
static const USBDescriptor *get_descriptor
	(
		USBDriver *usbp,
		uint8_t dtype,
		uint8_t dindex,
		uint16_t lang
	) {

	(void)usbp;
	(void)lang;
	switch (dtype) {
	case USB_DESCRIPTOR_DEVICE:
#if COMM_DEBUG_LEDS
		palSetPad(GPIOB, GPIOB_LEDB13);
#endif
		return &vcom_device_descriptor;
	case USB_DESCRIPTOR_CONFIGURATION:
#if COMM_DEBUG_LEDS
		palSetPad(GPIOB, GPIOB_LEDB12);
#endif
		return &vcom_configuration_descriptor;
	case USB_DESCRIPTOR_STRING:
#if COMM_DEBUG_LEDS
		palSetPad(GPIOB, GPIOB_LEDB11);
#endif
		if (dindex < sizeof(vcom_strings) / sizeof(*vcom_strings))
			return &vcom_strings[dindex];
	}
#if COMM_DEBUG_LEDS
	palSetPad(GPIOB, GPIOB_LEDB10);
#endif
	return NULL;
}

/// IN EP1 state.
static USBInEndpointState ep1instate;

/// OUT EP1 state.
static USBOutEndpointState ep1outstate;

/// EP1 initialization structure (both IN and OUT).
static const USBEndpointConfig ep1config = {
	USB_EP_MODE_TYPE_BULK,
	NULL,
	sduDataTransmitted,
	sduDataReceived,
	0x0040,
	0x0040,
	&ep1instate,
	&ep1outstate,
	1,
	NULL
};

/// IN EP2 state.
static USBInEndpointState ep2instate;

/// EP2 initialization structure (IN only).
static const USBEndpointConfig ep2config = {
	USB_EP_MODE_TYPE_INTR,
	NULL,
	sduInterruptTransmitted,
	NULL,
	0x0010,
	0x0000,
	&ep2instate,
	NULL,
	1,
	NULL
};

/// Handles the USB driver global events.
static void usb_event(USBDriver *usbp, usbevent_t event) {
	switch (event) {
	case USB_EVENT_RESET:
#if COMM_DEBUG_LEDS
		palSetPad(GPIOC, GPIOC_LEDC10);
#endif
		return;
	case USB_EVENT_ADDRESS:
#if COMM_DEBUG_LEDS
		palSetPad(GPIOC, GPIOC_LEDC11);
#endif
		return;
	case USB_EVENT_CONFIGURED:
#if COMM_DEBUG_LEDS
		palSetPad(GPIOC, GPIOC_LEDC12);
#endif
		chSysLockFromIsr();

		/* Enables the endpoints specified into the configuration.
		Note, this callback is invoked from an ISR so I-Class functions
		must be used.*/
		usbInitEndpointI(usbp, USBD1_DATA_REQUEST_EP, &ep1config);
		usbInitEndpointI(usbp, USBD1_INTERRUPT_REQUEST_EP, &ep2config);

		/* Resetting the state of the CDC subsystem.*/
		sduConfigureHookI(&SDU1);

		chSysUnlockFromIsr();
		return;
	case USB_EVENT_SUSPEND:
#if COMM_DEBUG_LEDS
		palSetPad(GPIOD, GPIOD_LEDD2);
#endif
		return;
	case USB_EVENT_WAKEUP:
#if COMM_DEBUG_LEDS
		palSetPad(GPIOC, GPIOC_LEDC1);
#endif
		return;
	case USB_EVENT_STALLED:
#if COMM_DEBUG_LEDS
		palSetPad(GPIOA, GPIOA_LEDA0);
#endif
		return;
	}
	return;
}

/// USB driver configuration.
static const USBConfig usbcfg = {
	usb_event,
	get_descriptor,
	sduRequestsHook,
	NULL
};

/// Serial over USB driver configuration.
static const SerialUSBConfig serusbcfg = {
	&USBD1,
	USBD1_DATA_REQUEST_EP,
	USBD1_DATA_AVAILABLE_EP,
	USBD1_INTERRUPT_REQUEST_EP
};

static CommSerialLine comm_usb_line;
static const CommSerial comm_usb_serial = {
	&comm_usb_line,COMM_SOURCE_USB,(BaseChannel*)&SDU1
};

bool_t commUsbIsConnected(void){
	return SDU1.config->usbp->state == USB_ACTIVE;
}

void commUsbReceiveAll(void){
	commSerialReceiveAll(&comm_usb_serial);
}

void commUsbTransmit(void){
	commSerialTransmit(&comm_usb_serial);
}

/// Initialize USB for Communication
void commUsbInit(void){
	sduObjectInit(&SDU1);
	sduStart(&SDU1, &serusbcfg);
	usbDisconnectBus(serusbcfg.usbp);
	chThdSleepMilliseconds(1000);
	usbStart(serusbcfg.usbp, &usbcfg);
	usbConnectBus(serusbcfg.usbp);
}
