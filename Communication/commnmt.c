/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "communication.h"
#include "commdrivers.h"

uint16_t comm_nmt_state = COMM_NMT_BOOTUP;

#if defined(__DOXYGEN__) || COMM_HEARTBEAT != 0
VirtualTimer comm_heartbeat_timer;
EventSource comm_heartbeat_event;
#endif

uint16_t commNmtStateI(void){
	return comm_nmt_state;
}

uint16_t commNmtState(void){
	chSysLock();
	uint16_t r = comm_nmt_state;
	chSysUnlock();
	return r;
}

void commNmtChangeState(uint16_t new_state){
	if(commNmtStateChanging(comm_nmt_state, new_state)){
		chSysLock();
		comm_nmt_state = new_state;
		chSysUnlock();
	}
}

void commHandleNmtFrame(void){
	switch(comm_frame.data8[0]){
	case 0x01:
		commNmtChangeState(COMM_NMT_OPERATIONAL);
		break;
	case 0x80:
		commNmtChangeState(COMM_NMT_PREOPERATIONAL);
		break;
	case 0x81:
		commNmtChangeState(COMM_NMT_RESET);
		break;
	}
}

#if defined(__DOXYGEN__) || COMM_HEARTBEAT != 0
static void commHeartbeatTimerElapsed(void* param){
	(void) param;
	chSysLockFromIsr();
	chEvtBroadcastFlagsI(&comm_heartbeat_event, 0);
	chVTSetI(&comm_heartbeat_timer, MS2ST(COMM_HEARTBEAT), &commHeartbeatTimerElapsed, NULL);
	chSysUnlockFromIsr();
}
void commHeartbeatInit(void){
	chEvtInit(&comm_heartbeat_event);
	chVTSet(&comm_heartbeat_timer, MS2ST(COMM_HEARTBEAT), &commHeartbeatTimerElapsed, NULL);
}
void commHeartbeatFini(void){
	chVTResetI(&comm_heartbeat_timer);
}
void commHandleHeartbeatEvent(void){
	comm_frame.sid = 0x700 + COMM_MODULE_ID;
	comm_frame.length = 1;
	comm_frame.data8[0] = comm_nmt_state & 0xFF;
	commDispatchFrame(COMM_SOURCE_MODULE);

	if(comm_nmt_state == COMM_NMT_BOOTUP)
		commNmtChangeState(COMM_NMT_PREOPERATIONAL);
}
#endif
