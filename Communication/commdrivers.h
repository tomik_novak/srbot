/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMDRIVERS_H_
#define COMMDRIVERS_H_

#include "communication.h"
#include "hal.h"

void commHandlePdoFrame(void);
void commHandleSdoFrame(void);
void commHandleNmtFrame(void);

#if defined(__DOXYGEN__) || COMM_ENABLE_CAN
/// Transmits the frame in comm_frame.
void commCanTransmit(void);
void commCanInit(void);
void commCanReceiveAll(void);
#endif

#if defined(__DOXYGEN__) || COMM_ENABLE_USB
extern SerialUSBDriver SDU1;
void commUsbTransmit(void);
void commUsbInit(void);
void commUsbReceiveAll(void);
#endif


#if defined(__DOXYGEN__) || COMM_SDO_TIMEOUT !=0
extern EventSource comm_sdo_timer_event;
void commHandleSdoTimerEvent(void);
void commSdoTimerInit(void);
#endif

extern uint16_t comm_nmt_state;

#if defined(__DOXYGEN__) || COMM_HEARTBEAT != 0
extern EventSource comm_heartbeat_event;
void commHandleHeartbeatEvent(void);
void commHeartbeatInit(void);
void commHeartbeatFini(void);
#endif

#endif /* COMMDRIVERS_H_ */

