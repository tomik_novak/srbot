/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "commtext.h"
#include "commusb.h"

char commTextToHexDigit(uint8_t p){
	if(p <= 9)
		return p + '0';
	if(p <= 16)
		return p - 10 + 'a';
	return '?';
}

uint8_t commTextFromHexDigit(char p){
	if(p >= '0' && p <= '9')
		return p - '0';
	if(p >= 'a' && p <= 'f')
		return p - 'a' + 10;
	if(p >= 'A' && p <= 'F')
		return p - 'A' + 10;
	return 255;
}

void prints(BaseSequentialStream* ch, const char* str){
	while(*str)
		chSequentialStreamPut(ch, (uint8_t)*str++);
}
void println(BaseSequentialStream* ch){
	chSequentialStreamPut(ch, '\r');
}
void printsln(BaseSequentialStream* ch, const char* str){
	prints(ch, str);
	println(ch);
}
void printx1(BaseSequentialStream* ch, uint8_t x1){
	chSequentialStreamPut(ch, commTextToHexDigit(x1 & 0xF));
}
void printx2(BaseSequentialStream* ch, uint8_t x2){
	chSequentialStreamPut(ch, commTextToHexDigit(x2 >> 4));
	chSequentialStreamPut(ch, commTextToHexDigit(x2 & 0xF));
}
void printx4(BaseSequentialStream* ch, uint16_t x4){
	printx2(ch, x4 >> 8);
	printx2(ch, x4 & 0xFF);
}
void printx8(BaseSequentialStream* ch, uint32_t x8){
	printx4(ch, x8 >> 16);
	printx4(ch, x8 & 0xFFFF);
}

void printxn(BaseSequentialStream* ch, uint32_t xn, uint32_t n){
	for(; n>0; --n){
		uint32_t shift = (n - 1) * 4;
		printx1(ch, xn >> shift);
	}
}
