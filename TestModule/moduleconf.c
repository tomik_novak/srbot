/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "communication.h"
#include "module.h"
#include "commcan.h"
#include "communicationtest.h"


const CommHandlerEntry comm_sdo_handlers[] = {
		{0x0101, &testSdoReadOnlyHandler,1},
		{0x0102, &testSdoReadOnlyHandler,2},
		{0x0103, &testSdoReadOnlyHandler,3},
		{0x0104, &testSdoReadOnlyHandler,4},
		{0x0105, &testSdoReadOnlyHandler,5},
		{0x0106, &testSdoReadOnlyHandler,6},
		{0x0107, &testSdoReadOnlyHandler,7},
		{0x0108, &testSdoReadOnlyHandler,8},
		{0x0109, &testSdoReadOnlyHandler,9},
		{0x010A, &testSdoReadOnlyHandler,10},
		{0x010B, &testSdoReadOnlyHandler,11},
		{0x010C, &testSdoReadOnlyHandler,12},
		{0x010D, &testSdoReadOnlyHandler,13},
		{0x010E, &testSdoReadOnlyHandler,14},
		{0x010F, &testSdoReadOnlyHandler,15},
		{0x0110, &testSdoReadOnlyHandler,16},

		{0x0201, &testSdoWriteOnlyHandler,1},
		{0x0202, &testSdoWriteOnlyHandler,2},
		{0x0203, &testSdoWriteOnlyHandler,3},
		{0x0204, &testSdoWriteOnlyHandler,4},
		{0x0205, &testSdoWriteOnlyHandler,5},
		{0x0206, &testSdoWriteOnlyHandler,6},
		{0x0207, &testSdoWriteOnlyHandler,7},
		{0x0208, &testSdoWriteOnlyHandler,8},
		{0x0209, &testSdoWriteOnlyHandler,9},
		{0x020A, &testSdoWriteOnlyHandler,10},
		{0x020B, &testSdoWriteOnlyHandler,11},
		{0x020C, &testSdoWriteOnlyHandler,12},
		{0x020D, &testSdoWriteOnlyHandler,13},
		{0x020E, &testSdoWriteOnlyHandler,14},
		{0x020F, &testSdoWriteOnlyHandler,15},
		{0x0210, &testSdoWriteOnlyHandler,16},

		{0x0301, &testSdoReadWriteHandler,1},
		{0x0302, &testSdoReadWriteHandler,2},
		{0x0303, &testSdoReadWriteHandler,3},
		{0x0304, &testSdoReadWriteHandler,4},
		{0x0305, &testSdoReadWriteHandler,5},
		{0x0306, &testSdoReadWriteHandler,6},
		{0x0307, &testSdoReadWriteHandler,7},
		{0x0308, &testSdoReadWriteHandler,8},
		{0x0309, &testSdoReadWriteHandler,9},
		{0x030A, &testSdoReadWriteHandler,10},
		{0x030B, &testSdoReadWriteHandler,11},
		{0x030C, &testSdoReadWriteHandler,12},
		{0x030D, &testSdoReadWriteHandler,13},
		{0x030E, &testSdoReadWriteHandler,14},
		{0x030F, &testSdoReadWriteHandler,15},
		{0x0310, &testSdoReadWriteHandler,16},

		{0x0401, &moduleConfigObjectHandler,(uint32_t)&(const ModuleConfigObject){test_objects[0], &test_mutex_1, (const uint8_t[]){4,8,12}, 3, NULL, 0}},
		{0x0402, &moduleConfigObjectHandler,(uint32_t)&(const ModuleConfigObject){test_objects[1], &test_mutex_1, (const uint8_t[]){10}, 1, NULL, 0}},
		{0x0403, &moduleConfigObjectHandler,(uint32_t)&(const ModuleConfigObject){test_objects[2], &test_mutex_2, (const uint8_t[]){1,3,6,10,15,21}, 6, NULL, 0}},
		{0x0404, &moduleConfigObjectHandler,(uint32_t)&(const ModuleConfigObject){test_objects[3], NULL, (const uint8_t[]){1}, 1, NULL, 0}},

		{0x3E01, &moduleResetHandler,0},
#if TEST_TOKEN
		{0x3E05, &testTokenHandler,0},
#endif
};
const CommHandlerEntry comm_pdo_handlers[] = {{0xFF, &testPdoHandler, 0}};
const uint32_t comm_sdo_handler_count=sizeof(comm_sdo_handlers)/sizeof(*comm_sdo_handlers);
const uint32_t comm_pdo_handler_count=sizeof(comm_pdo_handlers)/sizeof(*comm_pdo_handlers);

const CANFilter comm_can_filters[]={
	SIMPLE_2MASK_FILTER(0, 0x000, 0x000, 0x000, 0x000),
};
const uint32_t comm_can_filter_count=1;
