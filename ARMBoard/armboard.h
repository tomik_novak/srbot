/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ARMBOARD_H_
#define ARMBOARD_H_

/**
 * @file armboard.h
 *
 * This file is separated from board.h to avoid unnecessary include dependencies.
 */

#include "moduleconf.h"

#include "shields.h"

#if SHIELD_TOP_ID==SHIELD_ID_MOTOR || SHIELD_BOTTOM_ID==SHIELD_ID_MOTOR || defined(__DOXYGEN__)
#include "motorshield.h"
#endif

#if SHIELD_TOP_ID==SHIELD_ID_TEST || SHIELD_BOTTOM_ID==SHIELD_ID_TEST || defined(__DOXYGEN__)
#include "testshield.h"
#endif

#if SHIELD_TOP_ID==SHIELD_ID_NONE || SHIELD_BOTTOM_ID==SHIELD_ID_NONE || defined(__DOXYGEN__)
#include "noshield.h"
#endif

#if SHIELD_TOP_ID==SHIELD_ID_IR || SHIELD_BOTTOM_ID==SHIELD_ID_IR || defined(__DOXYGEN__)
#include "irshield.h"
#endif

#if SHIELD_TOP_ID==SHIELD_ID_SERVO || SHIELD_BOTTOM_ID==SHIELD_ID_SERVO || defined(__DOXYGEN__)
#include "servoshield.h"
#endif

/// @name Pins

/// USB power enable (push/pull, L) TODO: configurable?
#define GPIOA_USBEN				8
/// USB 5V (input floating)
#define GPIOA_VBUS				9
/// USB ID (input floating)
#define GPIOA_ID				10
/// USB D- (input floating)
#define GPIOA_USBDN				11
/// USB D+ (input floating)
#define GPIOA_USBDP				12
/// SWD IO (input floating)
#define GPIOA_SWDIO				13
/// SWD Clock (input floating)
#define GPIOA_SWDCLK			14

/// BOOT1 (input floating)
#define GPIOB_BOOT1				2
/// Can receive (input floating)
#define GPIOB_CAN_RX			8
/// Can transmit (alternate push/pull output, L)
#define GPIOB_CAN_TX			9

/// Orange LED (push/pull output)
#define GPIOC_LED14				7
/// Blue LED (push/pull output)
#define GPIOC_LED13				8
/// USB power fault (input floating)
#define GPIOC_USBFAULT			9

/// @}


/// @name Pin initialization
/// @{

#define VAL_BOARD_GPIOACR  0x0444444300000000ull
#define VAL_BOARD_GPIOAODR 0b0000000000000000

#define VAL_BOARD_GPIOBCR  0x000000B400000400ull
#define VAL_BOARD_GPIOBODR 0b0000000000000000

#define VAL_BOARD_GPIOCCR  0x4440004330000004ull
#define VAL_BOARD_GPIOCODR 0b0000000000000000

#define VAL_BOARD_GPIODCR  0x4444444444444044ull
#define VAL_BOARD_GPIODODR 0b0000000000000000
/// @}

#define VAL_GPIO(name) ((VAL_TOP_GPIO##name)|(VAL_BOTTOM_GPIO##name)|(VAL_BOARD_GPIO##name))
#define VAL_GPIO_ALL(name) {VAL_GPIO(name##ODR), (uint32_t)VAL_GPIO(name##CR) , (uint32_t)(VAL_GPIO(name##CR)>>32) }

void topInit(void);
void bottomInit(void);

#endif /* ARMBOARD_H_ */
