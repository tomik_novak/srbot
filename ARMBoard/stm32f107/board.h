/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio
    Robot Firmware - Modified 2014 by Vlastimil Dort

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
#ifndef BOARD_H_
#define BOARD_H_

/// Board identification macro
#define BOARD_ARMBOARD
/// Board identification string
#define BOARD_NAME              "ARMBoard"
/// External oscillator frequency
#define STM32_HSECLK            16000000
/// MCU type, defined in /ChibiOS/os/hal/platforms/STM32/stm32.h.
#define STM32F10X_CL

void boardInit(void);

#endif /* BOARD_H_ */
