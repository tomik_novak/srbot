/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio
    Robot Firmware - Modified 2014 by Vlastimil Dort

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/// This macro enables definitions in shield files, which should appear only
/// when included by this file. This is to enable remapping code to be
/// defined in shield header files.
#define BOARD_C_

#include "board.h"
#include "ch.h"
#include "hal.h"
#include "armboard.h"

#if HAL_USE_PAL || defined(__DOXYGEN__)
/// Default GPIO configuration specified in board.h and shield headers
const PALConfig pal_default_config = {
	VAL_GPIO_ALL(A), VAL_GPIO_ALL(B), VAL_GPIO_ALL(C), VAL_GPIO_ALL(D),
	{0xFFFFFFFF, 0x44444444, 0x44444444,} //< Port E is not available
};
#endif

void __early_init(void) {
	//Initialize clock as specified in mcuconf.h
	stm32_clock_init();
}

void boardInit(void) {
	//Remap the CAN1 to PB8 and PB9, disable JTAG
	AFIO->MAPR |= AFIO_MAPR_CAN_REMAP_REMAP2 | AFIO_MAPR_SWJ_CFG_JTAGDISABLE;

#ifdef SHIELD_INIT_TOP
	SHIELD_INIT_TOP();
#endif
#ifdef SHIELD_INIT_BOTTOM
	SHIELD_INIT_BOTTOM();
#endif
}
