# ARMBoard

This directory contains header files for using the ARMBoard hardware in ChibiOS.
It does not contain board schematics.

ARMBoard is a project with support for ARMBoard initialization and peripheral names.

## Adding support for shields

* Add a new constant (shield id) to shields.h.
* Add a new header file.
* In armboard.h, include the header file if `SHIELD_TOP_ID` or `SHIELD_BOTTOM_ID` is equal to the shield id.
* In the header file, define names of the pins or peripherals available on the shield.

### Pin initialization

In the header file, initialize pins by defining constants like `VAL_TOP_GPIOACR` or `VAL_TOP_GPIOAODR`.

The `VAL_*_GPIO*CR` values are hexadecimal pin modes, the `VAL_*_GPIO*ODR` values are binary initial values.
The lowest (0) pin is on the right, the highest (15) is on the left.

In `VAL_TOP_GPIO**`, initialize only the pins marked `T` in the table, if `SHIELD_TOP_ID` equals the shield id.
In `VAL_BOTTOM_GPIO**`, initialize only the pins marked `B` in the table, if `SHIELD_BOTTOM_ID` equals the shield id.
The pins marked `R` are initialized by the board, pins with `N` are not connected.

The values from both shields and the board are then ORed together.

port|15|14|13|12|11|10|9|8|7|6|5|4|3|2|1|0
-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-
GPIOA|B|R|R|R|R|R|R|R|T|T|B|B|B|T|T|T
GPIOB|B|B|T|T|T|T|R|R|B|B|B|B|B|R|B|B
GPIOC|N|N|N|T|T|T|R|R|R|B|T|T|B|B|T|N
GPIOD|N|N|N|N|N|N|N|N|N|N|N|N|N|T|N|N

Usual mode values:

Value|Mode
-|-
3|Digital push-pull output
4|Digital floating input
B|Alternate push-pull output

#### Example (initializing all pins of top shield to input floating)

```c
#define VAL_TOP_GPIOACR  0x0000000044000444ull
#define VAL_TOP_GPIOAODR 0b0000000000000000
#define VAL_TOP_GPIOBCR  0x0044440000000000ull
#define VAL_TOP_GPIOBODR 0b0000000000000000
#define VAL_TOP_GPIOCCR  0x0004440000440040ull
#define VAL_TOP_GPIOCODR 0b0000000000000000
#define VAL_TOP_GPIODCR  0x0000000000000400ull
#define VAL_TOP_GPIODODR 0b0000000000000000
```

### Alternate function remapping

If the shield uses remapped alternate pin functions, the alternate function should be remapped on board initialization.
To do this, test whether `BOARD_C_` is defined in the shield header file. If yes, define `SHIELD_INIT_BOTTOM` or `SHIELD_INIT_TOP` to
a name of a function and define the function to do the remapping.

#### Example (remapping UART1 on bottom shield):
```c
#ifdef BOARD_C_
#define SHIELD_INIT_BOTTOM shieldBottomInit
static void shieldBottomInit(void) {
	AFIO->MAPR |= AFIO_MAPR_USART1_REMAP;
}
#endif
``` 

## Adding support for MCUs

* Add a new directory with the name of the MCU in lowercase.
* Add board.h, board.c, board.mk and OpenOCD configuration file (stlink-v2.cfg).