/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio
    Robot Firmware - Modified 2014 by Vlastimil Dort
    Robot Firmware - Modified 2014 by Tomas Novak

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
#ifndef BOARD_H_
#define BOARD_H_

/// Board identification macro
#define BOARD_SRBOT
/// Board identification string
#define BOARD_NAME              "SRbot"
/// External oscillator frequency
#define STM32_HSECLK            16000000
/// MCU type, defined in /ChibiOS/os/hal/platforms/STM32/stm32.h.
#define STM32F10X_CL

/// USB 5V (input floating)
#define GPIOA_VBUS				9
/// USB ID (input floating)
//#define GPIOA_ID				10
/// USB D- (input floating)
#define GPIOA_USBDN				11
/// USB D+ (input floating)
#define GPIOA_USBDP				12
/// SWD IO (input floating)
#define GPIOA_SWDIO				13
/// SWD Clock (input floating)
#define GPIOA_SWDCLK			14

/// BOOT1 (input floating)
#define GPIOB_BOOT1				2
/// Can receive (input floating)
//#define GPIOB_CAN_RX			8
/// Can transmit (alternate push/pull output, L)
//#define GPIOB_CAN_TX			9

/// Orange LED (push/pull output)
#define GPIOC_LED14				5
/// Blue LED (push/pull output)
#define GPIOC_LED13				4
/// USB power fault (input floating)
//#define GPIOC_USBFAULT			9

#define GPIOA_LED12				7

#define GPIOA_P10				5
#define GPIOA_P9				6
#define GPIOC_P8				0
#define GPIOC_P7				1
#define GPIOC_P6				2
#define GPIOB_P1				12
#define GPIOB_P2				13
#define GPIOB_P3				14
#define GPIOC_P4				13
#define GPIOC_P5				14

#define GPIOA_ENCA				3
#define GPIOA_ENCB				15

#define GPIOA_PROX2				4
#define GPIOB_PROX4				9
#define GPIOB_PROX3				15
#define GPIOC_PROX1				15

#define GPIOC_PWM1				9
#define GPIOC_PWM2				8
#define GPIOC_PWM3				7
#define GPIOC_PWM4				6

#define GPIOB_LD1				6
#define GPIOB_LD2				7
#define GPIOB_LD3				8
#define GPIOC_LD4				12
#define GPIOD_LD5				2


/// @}

/*
 * I/O ports initial setup, this configuration is established soon after reset
 * in the initialization code.
 *
 * The digits have the following meaning:
 *   0 - Analog input.
 *   1 - Push Pull output 10MHz.
 *   2 - Push Pull output 2MHz.
 *   3 - Push Pull output 50MHz.
 *   4 - Digital input.
 *   5 - Open Drain output 10MHz.
 *   6 - Open Drain output 2MHz.
 *   7 - Open Drain output 50MHz.
 *   8 - Digital input with PullUp or PullDown resistor depending on ODR.
 *   9 - Alternate Push Pull output 10MHz.
 *   A - Alternate Push Pull output 2MHz.
 *   B - Alternate Push Pull output 50MHz.
 *   C - Reserved.
 *   D - Alternate Open Drain output 10MHz.
 *   E - Alternate Open Drain output 2MHz.
 *   F - Alternate Open Drain output 50MHz.
 * Please refer to the STM32 Reference Manual for details.
 */
/// @name Pin initialization
/// @{

#define VAL_BOARD_GPIOACR  0x4444434333384444ull
#define VAL_BOARD_GPIOAODR 0b0000010100010000

#define VAL_BOARD_GPIOBCR  0x8333448333444444ull
#define VAL_BOARD_GPIOBODR 0b1000001000000000

#define VAL_BOARD_GPIOCCR  0x8333449999334333ull
#define VAL_BOARD_GPIOCODR 0b1000000000000000

#define VAL_BOARD_GPIODCR  0x4444444444444344ull
#define VAL_BOARD_GPIODODR 0b0000000000000000
/// @}

#define VAL_GPIO(name) ((VAL_BOARD_GPIO##name))
#define VAL_GPIO_ALL(name) {VAL_GPIO(name##ODR), (uint32_t)VAL_GPIO(name##CR) , (uint32_t)(VAL_GPIO(name##CR)>>32) }


void boardInit(void);

#endif /* BOARD_H_ */
