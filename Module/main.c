/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ch.h"
#include "hal.h"
#include "module.h"
#include "communication.h"
#include "commserial.h"
#include "switch.h"

uint16_t reset_reason;

int main(void){
	PWR->CR |= PWR_CR_DBP;
	//Read reset reason before ChibiOS clears it
	reset_reason = BKP->DR41;

	halInit();
	chSysInit();
	moduleInit();
	commStartAll();
	moduleMain();
	return 0;
}

void commTerminated(void){
	moduleResetWithReason(reset_reason);
}

void commSerialUserCommand(const CommSerial* serial){
	if(serial->line->data[0] == 'l'){
		if(serial->line->data[1] == 'a'){
			moduleResetWithReason(SWITCH_REASON_APPLICATION_REQUEST);
		}
		else if(serial->line->data[1] == 'l'){
			moduleResetWithReason(SWITCH_REASON_LOADER_REQUEST);
		}
	}
	moduleSerialCommand(serial);
}

__attribute__((weak))
void moduleSerialCommand(const CommSerial* serial){
	(void)serial;
}

void moduleResetHandler(CommHandlerEvent event){
	switch(event){
	case COMM_SDO_WRITE_START:
		if(comm_sdo.size != 2)
			comm_sdo.error = COMM_SDO_ERROR_TYPE;
		if(comm_sdo.object_subindex != 0)
			comm_sdo.error = COMM_SDO_ERROR_SUBINDEX;
		break;
	case COMM_SDO_WRITE_DATA:
		reset_reason = comm_sdo.data16[0];
		break;
	case COMM_SDO_READ_START:
		comm_sdo.size = 2;
		break;
	case COMM_SDO_READ_DATA:
		comm_sdo.data16[0] = reset_reason;
		break;
	default:
		break;
	}
}

static uint8_t module_sdo_buffer[MODULE_MAX_CONFIG_OBJECT_SIZE];

static void moduleConfigObjectComputeSize(uint32_t* size, uint32_t* offset){
	const ModuleConfigObject* config_object = (const ModuleConfigObject*) comm_sdo_handler->param;
	if(comm_sdo.object_subindex){
		if(comm_sdo.object_subindex == 1)
			*offset = 0;
		else
			*offset = config_object->offsets[comm_sdo.object_subindex - 2];
		*size = config_object->offsets[comm_sdo.object_subindex - 1] - *offset;
	}
	else{
		*offset = 0;
		*size = config_object->offsets[config_object->parts - 1];
	}
}

void moduleConfigObjectHandler(CommHandlerEvent event){
	const ModuleConfigObject* config_object = (const ModuleConfigObject*) comm_sdo_handler->param;
	uint32_t size, offset;

	switch(event){
	case COMM_SDO_READ_START:
		comm_sdo.size = 4;
		if(comm_sdo.object_subindex > config_object->parts){
			comm_sdo.error = COMM_SDO_ERROR_SUBINDEX;
			return;
		}
		moduleConfigObjectComputeSize(&size, &offset);
		comm_sdo.size = size;
		if(config_object->mutex)
			chMtxLock(config_object->mutex);
		else
			chSysLock();
		commCopy(module_sdo_buffer, (uint8_t*)config_object->data + offset, size);
		if(config_object->mutex)
			chMtxUnlock();
		else
			chSysUnlock();
		break;
	case COMM_SDO_READ_DATA:
		commCopy(comm_sdo.data8, module_sdo_buffer + comm_sdo.index, comm_sdo.segment);
		break;
	case COMM_SDO_WRITE_START:
		if(comm_sdo.object_subindex > config_object->parts){
			comm_sdo.error = COMM_SDO_ERROR_SUBINDEX;
			return;
		}
		moduleConfigObjectComputeSize(&size, &offset);
		if(comm_sdo.size > size)
			comm_sdo.error = COMM_SDO_ERROR_LENGTH_HIGH;
		else if(comm_sdo.size < size)
			comm_sdo.error = COMM_SDO_ERROR_LENGTH_LOW;

		break;
	case COMM_SDO_WRITE_DATA:
		commCopy(module_sdo_buffer + comm_sdo.index, comm_sdo.data8, comm_sdo.segment);
		break;
	case COMM_SDO_WRITE_END:
		moduleConfigObjectComputeSize(&size, &offset);
		if(config_object->mutex)
			chMtxLock(config_object->mutex);
		else
			chSysLock();
		commCopy((uint8_t*)config_object->data + offset, module_sdo_buffer, size);
		if(config_object->mutex)
			chMtxUnlock();
		else
			chSysUnlock();

		if(config_object->event_source)
			chEvtBroadcastFlags(config_object->event_source, config_object->event_flags);
		break;
	default:
		break;
	}
}
