/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MODULE_H_
#define MODULE_H_

#include "moduleconf.h"
#include "ch.h"
#include "commserial.h"

#if defined(__DOXYGEN__) || !defined(MODULE_MAX_CONFIG_OBJECT_SIZE)
/// Maximum size of configuration object (size of a buffer to hold it).
#define MODULE_MAX_CONFIG_OBJECT_SIZE 64
#endif


/// Configuration object specification.
/// You can use this syntax:
/// http://gcc.gnu.org/onlinedocs/gcc/Compound-Literals.html
typedef struct{
	/// Pointer to the object where the value is stored.
	/// The object should be only accessed with proper synchronization.
	/// The object must have exactly the same layout as the data of the SDO,
	/// you should only use packed structures.
	/// Must not be ::NULL.
	void* data;
	/// Mutex used for synchronization of accesses to the object.
	/// You can use the same mutex for multiple objects.
	/// Can be ::NULL.
	/// If NULL, chSysLock is used instead of a mutex.
	Mutex* mutex;
	/// Array of offsets to parts of the object.
	/// The array must have the same length as there are parts in the object.
	/// The elements correspond to the offset of the END of each part.
	/// (so the last element is the size of the whole object).
	/// Must not be ::NULL.
	const uint8_t* offsets;
	/// Number of parts of the object.
	/// Must be at least 1.
	uint8_t parts;
	/// Event source which will be signaled on object update.
	EventSource* event_source;
	/// Flags added to the event on object update.
	flagsmask_t event_flags;
} ModuleConfigObject;

/// SDO handler for configuration objects.
/// The param should be a pointer to ModuleConfigObject structure.
void moduleConfigObjectHandler(CommHandlerEvent event);

/// Reset the module.
/// Performs software reset of the microcontroller.
/// @param reason The value that should be stored in backup memory before reset.
///               For valid values see Switch/switch.h
void moduleResetWithReason(uint16_t reason);

/// Handler for reset SDO.
void moduleResetHandler(CommHandlerEvent);

/// Initialize module before starting communication.
extern void moduleInit(void);
/// Main module procedure. Everything is initialized when called.
extern void moduleMain(void);
/// Handle serial command.  Implement only if needed.
extern void moduleSerialCommand(const CommSerial* serial);

#endif /* MODULE_H_ */
