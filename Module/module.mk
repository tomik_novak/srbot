# Robot Firmware - Modified 2014 by Vlastimil Dort

# This file should be included at the end of makefiles of modules.

# Include features common to all modules
include $(WORKSPACE)/Communication/feature.mk

# C sources that can be compiled in ARM or THUMB mode depending on the global
# setting.
CSRC = $(PORTSRC) \
       $(KERNSRC) \
       $(HALSRC) \
       $(PLATFORMSRC) \
       $(BOARDSRC) \
       $(MODULESRC) \
       $(FEATURESRC) \
       $(WORKSPACE)/Module/main.c $(RESETSRC)

# C++ sources that can be compiled in ARM or THUMB mode depending on the global
# setting.
CPPSRC = $(MODULECPPSRC)


# List ASM source files here
ASMSRC = $(PORTASM)

INCDIR = $(PORTINC) $(KERNINC) \
         $(HALINC) $(PLATFORMINC) $(BOARDINC) $(MODULEINC) $(FEATUREINC)\
         $(WORKSPACE)/Module $(WORKSPACE)/Switch\
         $(CHIBIOS)/os/various 


MCU  = cortex-m3
TRGT = $(ARMGCC)/bin/arm-none-eabi-
CC   = $(TRGT)gcc
CPPC = $(TRGT)g++
LD   = $(TRGT)gcc
CP   = $(TRGT)objcopy
AS   = $(TRGT)gcc -x assembler-with-cpp
OD   = $(TRGT)objdump
HEX  = $(CP) -O ihex
BIN  = $(CP) -O binary

# THUMB-specific options here
TOPT = -mthumb -DTHUMB

# Define C warning options here
CWARN = -Wall -Wextra -Wstrict-prototypes

# Define C++ warning options here
CPPWARN = -Wall -Wextra

include $(CHIBIOS)/os/ports/GCC/ARMCMx/rules.mk

##
# Configuration
##

# If the specified configuration is different from the saved configuration,
# clean the project, and forbid making "all".
ifeq ($(foreach x,$(CONFIGS),$(LOCAL_$(x))),$(foreach x,$(CONFIGS),$(CONF_$(x))))
config: localconfig
else
config: distclean localconfig
all: FAIL
endif

# Save the specified configuration into "local.mk" file.
# The saved configuration will be used by the next make invocation.
localconfig: 
	@echo $(foreach x,$(CONFIGS),"LOCAL_$(x)=$(CONF_$(x))\n") > local.mk

# Clean the project including configuration
distclean: clean
	@echo "Cleaning configuration"
	rm -f local.mk
	@echo "Done"

.PHONY: config distclean
.DEFAULT_GOAL=all