/*
	Robot Firmware - Copyright (C) 2014 Vlastimil Dort

	This file is part of Robot Firmware.

	Robot Firmware is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Robot Firmware is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Robot Firmware.  If not, see <http://www.gnu.org/licenses/>.
*/

// Include headers
#include "srbottest.h"
#include "srbothal.h"
#include "board.h"
#include "hal.h"

static const uint32_t ir_bases[] = {
	GPIOC_BASE,GPIOC_BASE,GPIOC_BASE,
	GPIOA_BASE,GPIOA_BASE,
	GPIOB_BASE,GPIOB_BASE,GPIOB_BASE,
	GPIOC_BASE,GPIOC_BASE,	
/*	GPIOB_BASE,GPIOB_BASE,GPIOB_BASE,
	GPIOC_BASE,GPIOD_BASE,*/
};
static const uint32_t ir_offsets[] = {
	GPIOC_P8,
	GPIOC_P7,
	GPIOC_P6,
	GPIOA_P10,
	GPIOA_P9,
	GPIOB_P3,
	GPIOB_P2,
	GPIOB_P1,
	GPIOC_P4,
	GPIOC_P5,
/*	GPIOB_LD1,GPIOB_LD2,GPIOB_LD3,
	GPIOC_LD4,GPIOD_LD5,*/
};
static const uint32_t ld_bases[] = {
	GPIOB_BASE,GPIOB_BASE,GPIOB_BASE,
	GPIOC_BASE,GPIOD_BASE,
};
static const uint32_t ld_offsets[] = {
	GPIOB_LD1,GPIOB_LD2,GPIOB_LD3,
	GPIOC_LD4,GPIOD_LD5,
};

enum{
	IR_LED_COUNT = sizeof(ir_bases) / sizeof(*ir_bases),
	LD_LED_COUNT = sizeof(ld_bases) / sizeof(*ld_bases)
};

void testShieldTestLeds(void){
	int i;
#if SRBOTTEST_IRLEDS == TRUE
	for(i = 0; i < IR_LED_COUNT; ++i){
		palSetPad(((GPIO_TypeDef *)ir_bases[i]), ir_offsets[i]);
		chThdSleepMilliseconds(250);
		palClearPad(((GPIO_TypeDef *)ir_bases[i]) , ir_offsets[i]);
	}
#endif

#if SRBOTTEST_LDLEDS == TRUE
	for(i = 0; i < LD_LED_COUNT; ++i){
		palSetPad(((GPIO_TypeDef *)ld_bases[i]), ld_offsets[i]);
		chThdSleepMilliseconds(250);
		palClearPad(((GPIO_TypeDef *)ld_bases[i]) , ld_offsets[i]);
	}
#endif

#if SRBOTTEST_LEDS == TRUE
	greenLedOn();
	chThdSleepMilliseconds(100);
	greenLedOff();
	chThdSleepMilliseconds(100);
	blueLedToggle();
	chThdSleepMilliseconds(500);
	orangeLedToggle();
#endif
#if SRBOTTEST_ENCODERS == TRUE
	if(getLeftEnc())
	  greenLedOff();
	else
	  greenLedOn();
	
	if(getRightEnc())
	  orangeLedOff();
	else
	  orangeLedOn();

#endif
#if SRBOTTEST_FRONTBACKIRR == TRUE

	
	if(palReadPad((GPIO_TypeDef *)GPIOC_BASE,GPIOC_PROX1))
	  greenLedOff();
	else
	  greenLedOn();
	if(palReadPad((GPIO_TypeDef *)GPIOB_BASE,GPIOB_PROX3))
	  blueLedOff();
	else
	  blueLedOn();

#endif
#if SRBOTTEST_LEFTRIGHTIRR == TRUE

	if(!getLeftIRR())
	  greenLedOff();
	else
	  greenLedOn();

	
	if(palReadPad((GPIO_TypeDef *)GPIOB_BASE,GPIOB_PROX4))
	  blueLedOff();
	else
	  blueLedOn();
	

#endif

	
	chThdSleepMilliseconds(1);
}

void testShieldTestMain(void){
	for(;;)
		testShieldTestLeds();
}
