# Example module project

## Create a new module in Eclipse

*File* > *New* > *C Project*
*Project type*: *Makefile project* > *Empty project*
*Toolchain*: *Cross ARM GCC*

### Project properties

Set the encoding to `UTF-8` and line delimiter to Unix.
Alternatively, you can copy the `.settings/org.eclipse.core.runtime.prefs`
and `.settings/org.eclipse.core.resources.prefs` from this project.

### Module files

Most of the functionality of the module should be kept in separate projects, features, to
allow their reuse.
Module brings those features together and configures them.

#### Configuration directories

The module can supply multiple predefined configurations.
For example for each robot competition, you should add a new predefined configuration so that 
the module works in the robot for the competition. Also, use predefined configurations to distinguish
between multiple instances of the module on the same robot, or if you use more robots.
For example, you can name the configuration directory like `competition_robot_instance` (`s14_a_0`, `s14_a_1`, ...).

For each predefined configuration, create a directory and add `build.mk` to it.
You must set the variable `MODULE_CONF=ab2f107` to select the target hardware.  
You can set other make variables, which you can then use in Makefile.
Usually, you would use them to set `UDEFS` or `UINCDIR`. For example,
you can add the directory to `UINCDIR` and then for each configuration create different header files.  

The predefined configuration is selected by `make config CONF_BUILD=s14_a_0`. 

#### Makefile

Copy the makefile from this project.
You should include `feature.mk` from every feature used by the project.

#### GIT 

You must add `/.dep`, `/build`, and `/local.mk` to `.gitignore` (or copy it from this project).

#### `moduleconf.h`

`moduleconf.h` is used by all features and board files.
You can, for example, override the macros from `chconf.h` or `halconf.h` or `communication.h` from it.
You should select which shields the module uses.
It should only contain preprocessor defines.

For some settings, you may want to use different values for different robots. To do that, place `moduleconf.h` to each configuration directory.
You can include a common header file to share settings between more configurations.

The names of header files in module projects do not have to be unique among the whole workspace,
because modules are never referenced by each other.

#### `.c` files

You can create any number of `.c` files.

You should implement:

* `void moduleInit(void)`
   * called before communication initialization
* `void moduleMain(void)`
   * called after communication initialization
   * should contain the main code (infinite loop)
* `bool_t commNmtStateChanging(uint16_t old, uint16_t new)`
   * handle NMT state change (arguments are from `enum CommNmtState`)
   * return TRUE to allow the state change
* `const CommHandlerEntry comm_sdo_handlers[]`
* `const uint32_t comm_sdo_handler_count`
* `const CommHandlerEntry comm_pdo_handlers[]`
* `const uint32_t comm_pdo_handler_count`
* `const CANFilter comm_can_filters[]`
* `const uint32_t comm_can_filter_count`

You can optionally implement:

* `void moduleSerialCommand(const CommSerial* serial)`
   * handle custom serial (or USB) commands

#### SDO objects

For SDO objects, you should usually use `moduleConfigObjectHandler`.